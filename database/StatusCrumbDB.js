const UserDB = require('../database').UserDB;
const Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {
    var StatusCrumbDB = sequelize.define('statusCrumb', {
        statusCode: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.GEOMETRY('POINT')
        },
        locationDesc: {
            type: Sequelize.STRING
        }
    }, {
        classMethods: {

        },
        instanceMethods: {

        }
    });

    StatusCrumbDB.belongsTo(UserDB, { as:'User', foreignKey: 'username', foreignKeyConstraint:true});

    // force: true will drop the table if it already exists
    StatusCrumbDB.sync({force: false}).then(function () {
        // do nothing;
    });

    return StatusCrumbDB;
};
