const UserDB = require('../database').UserDB;
const Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {

    function injectLatestStatusCrumb(message, options) {
        return UserDB.findOne({
            where: {
                username: message.author
            },
            attributes: ['statusCode']
        }).then(function(user){
            message.statusCode = user.statusCode;
        });
    }

    var MessageDB = sequelize.define('message', {
        content: {
            type: Sequelize.STRING
        },
        messageType: {
            type: Sequelize.STRING
        },
        postedAt: {
            type: Sequelize.DATE
        },
        location: {
            type: Sequelize.GEOMETRY('POINT')
        },
        statusCode: {
            type: Sequelize.STRING
        }
    }, {
        classMethods: {

        },
        instanceMethods: {
        },
        hooks: {
            beforeCreate: injectLatestStatusCrumb
        }
    }
    );

    MessageDB.belongsTo(UserDB, { as:'AuthorUser', foreignKey: 'author', foreignKeyConstraint:true});
    MessageDB.belongsTo(UserDB, { as:'ReceiverUser', foreignKey: 'receiver', foreignKeyConstraint:true});

    // force: true will drop the table if it already exists
    MessageDB.sync({force: false}).then(function () {
        // do nothing;
    });

    return MessageDB;
};
