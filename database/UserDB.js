const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const reservedUsernames = require('../helpers/ReservedUsernameList');
const onlineStates = require('../helpers/UserOnlineStates');

function hashPassword(password) {
    return bcrypt.hash(password, 10);
}


module.exports = function(sequelize, DataTypes) {
    var hashPasswordHook = function (user, options) {
        if (!user.changed('password')) return;

        return hashPassword(user.password)
            .then(function(hash){
                user.setDataValue("password", hash);
            });
    };

    var UserDB = sequelize.define('user', {
        username: {
            type: Sequelize.STRING, unique: true, primaryKey: true
        },
        password: {
            type: Sequelize.STRING
        },
        lastLoginAt: {
            type: Sequelize.DATE
        },
        statusCode: {
            type: Sequelize.STRING,
            defaultValue: "UNDEFINED"
        },
        twitterToken: {
            type: Sequelize.STRING,
        },
        twitterTokenSecret: {
            type: Sequelize.STRING,
        },
        latitude: {
            type: Sequelize.DOUBLE
        },
        longitude: {
            type: Sequelize.DOUBLE
        },
        shareFootprintsSetting: {
            type: Sequelize.INTEGER,
            defaultValue: 1 // 0 = no, 1 = yes, 2 = auto
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        privilegeLevel: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    }, {
        // TODO: migrate to ES6 style when migrating to sequelize 4.0
        classMethods: {
            hashPassword: function(password) {
                return hashPassword(password);
            }
        },
        instanceMethods: {
            validatePassword: function(password, callback) {
                bcrypt.compare(password, this.password).then(callback);
            },
            updateLastLoginAt: function() {
                this.lastLoginAt = new Date();
                this.save();
            },
            // DO NOT CALL THIS UNLESS IT'S THE LAST METHOD IN THE REQUEST
            safeOutput: function () {
                var data = this.dataValues;
                delete data.password;
                data.isOnline = onlineStates.isOnline(data.username);
                return data;
            },
            updateStatusCode: function(statusCode) {
                return this.update({ statusCode: statusCode});
            },
            updateLocation: function(location) {
                if(location !== null) {
                    return this.update({ latitude: location.coordinates[1], longitude: location.coordinates[0]});
                }
            }
        },
        hooks: {
            beforeCreate: hashPasswordHook
        }
    });


    // force: true will drop the table if it already exists
    UserDB.sync({force: false}).then(function () {
        UserDB.findOrCreate({
            where: {username: "ESNAdmin"},
            defaults: {username: "ESNAdmin", password: "admin", privilegeLevel: 2, statusCode: "GREEN"}
        });
    });

    return UserDB;
};
