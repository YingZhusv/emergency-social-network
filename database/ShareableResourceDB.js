const Sequelize = require('sequelize');
const UserDB = require('../database').UserDB;

module.exports = function(sequelize, DataTypes) {

    var ShareableResourceDB = sequelize.define('shareableResource', {
        types: {
            type: Sequelize.STRING // Comma-separated FOOD, WATER, MEDKIT and SHELTER
        },
        description: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.GEOMETRY('POINT')
        }
    }, {
        classMethods: {
        },
        instanceMethods: {
        }
    });

    ShareableResourceDB.belongsTo(UserDB, { as:'User', foreignKey: 'username', foreignKeyConstraint:true});

    // force: true will drop the table if it already exists
    ShareableResourceDB.sync({force: false}).then(function () {
        // do nothing;
    });

    return ShareableResourceDB;
};
