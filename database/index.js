const Sequelize = require('sequelize');

console.log('>>>>>> Initialize database');

// createdAt and updatedAt are automatically added
const dbName = process.env.USE_TEST_DB ? process.env.TEST_DB_NAME : process.env.DB_NAME;
const sequelize = new Sequelize(dbName, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    logging: false,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    dialectOptions: {
        ssl: process.env.DB_USE_SSL === 'true'
    }
});
sequelize.authenticate()
    .then(function(err) {
        console.log('Connection has been established successfully.');
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err);
    });

// load models
var models = [
    'UserDB',
    'StatusCrumbDB',
    'MessageDB',
    'ShareableResourceDB'
];

models.forEach(function(model) {
    module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// export connection
module.exports.sequelize = sequelize;
