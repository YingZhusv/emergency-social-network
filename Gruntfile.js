const coverageFolder = process.env.CIRCLE_TEST_REPORTS === undefined ?
    'coverage' : process.env.CIRCLE_TEST_REPORTS + '/coverage';

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        env: {
            local: {
                src: '.env',
                USE_TEST_DB: true
            },
            circleci: {
                NODE_ENV: 'production',
                PORT: '3000',
                SESSION_SECRET: 'circle_test',
                DB_NAME: 'circle_test',
                DB_USERNAME: 'ubuntu',
                DB_PASSWORD: '',
                DB_HOST: 'localhost',
                DB_PORT: '5432',
                DB_USE_SSL: 'false',
                CALL_BACK_URL: 'http://localhost:3000'
            }
        },

        jshint: {
            files: [
                'Gruntfile.js',
                'server.js',
                'controllers/**/*.js',
                'database/**/*.js',
                'helpers/**/*.js',
                'models/**/*.js',
                'public/**/*.js',
                'routes/**/*.js',
                'test/**/*.js'
            ],
            options: {
                esversion: 6,
                node: true,
                expr: true
            }
        },

        mochaTest: {
            local: {
                options: {
                    reporter: 'spec',
                    //captureFile: 'results.txt', // Optionally capture the reporter output to a file
                    quiet: false, // Optionally suppress output to standard out (defaults to false)
                    clearRequireCache: false, // Optionally clear the require cache before running tests (defaults to false)
                    ui: 'tdd'
                },
                src: ['test/**/*.js']
            },
            circleci: {
                options: {
                    ui: 'tdd',
                    reporter: 'mocha-junit-reporter',
                    quiet: false,
                    reporterOptions: {
                        mochaFile: process.env.CIRCLE_TEST_REPORTS + '/mocha/results.xml'
                    }
                },
                src: ['test/**/*.js']
            },
        },

        mocha_istanbul: {
            coverage: {
                src: ['test/**/*.js'], // a folder works nicely
                options: {
                    mochaOptions: ['--ui', 'tdd'], // any extra options for mocha
                    istanbulOptions: ['--dir', coverageFolder]
                }
            }
        }

    });


    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // Load the plugin that provides the "uglify" task.
    // grunt.loadNpmTasks('grunt-mocha'); Client Side testing
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-mocha-istanbul');

    // Default task(s).
    grunt.registerTask('default', []);

    // Test
    grunt.registerTask('test', ['env:local', 'mochaTest:local']);

    // Coverage
    grunt.registerTask('coverage', ['mocha_istanbul']);

    // CircleCI
    grunt.registerTask('circleci', ['env:circleci', 'mochaTest:circleci', 'mocha_istanbul']);

};
