const Message = require('../models/Message');

class AnnouncementController {
    static postAnnouncement(req, res, next) {
        let username = req.user.username;
        let content = req.body.content;

        Message.postAnnouncement(username, content)
            .then(function(message) {
                if (message) {
                    res.status(201).json(message.dataValues);
                }
            }).catch(function (err) {
                console.log(err);
                next(err);
            });
    }

    static getAnnouncements(req, res, next) {
        Message.getAnnouncements()
            .then(function(messages) {
                res.json(messages);
            });
    }
}

module.exports = AnnouncementController;
