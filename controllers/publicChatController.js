const Message = require('../models/Message');

const socket = require('./socketController');
const onlineStates = require('../helpers/UserOnlineStates');

class PublicChatController {

    static postMessage(req, res, next) {
        // Configure location object
        let location = null;
        let username = req.user.username;
        let content = req.body.content;
        if ('latitude' in req.body && 'longitude' in req.body) {
            location = {
                type: 'POINT',
                coordinates: [req.body.longitude, req.body.latitude]
            };
        }

        Message.postPublicMessage(username, content, location)
        .then(function(message) {
            if (message) {
                var authorSocket = onlineStates.getSocket(message.author, req.body.socketId);

                var socketMessageBuilder = authorSocket !== null ? authorSocket : socket.io;
                socketMessageBuilder.in("Public").emit("newMessage", message);

                res.status(201).json(message.dataValues);
            }
        }).catch(function (err) {
            console.log(err);
            next(err);
        });
    }

    static getMessages(req, res, next) {
        Message.getPublicMessages()
        .then(function(messages) {
            res.json(messages);
        });
    }

    static getMessagesByUsername(req, res, next) {
        var username = req.params.username;
        Message.getPublicMessagesByUsername(username)
        .then(function(messages) {
            res.json(messages);
        });
    }

}

module.exports = PublicChatController;
