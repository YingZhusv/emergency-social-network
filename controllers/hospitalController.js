const StatusCrumb = require('../models/StatusCrumb');
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyAe8U2H9dfiySKWEEPiwQSmJHTjtM2DLRI'
});
const request = require('request');
const server_token = 'Token um4VMzjmn_HN4t_VshviRJA-m0qJFzWqgRi4Ar9R';

class HospitalController {

    static getHospitalNearby(req, res, next) {

        let lat = req.query.start_latitude;
        let lnt = req.query.start_longitude;

        googleMapsClient.placesNearby({
          language: 'en',
          location: [lat, lnt],
          radius: 5000,
          type: 'hospital'
        }, (err, response) => {
            if (!err)
                res.send(response.json.results);
            else {
                next(err);
            }
        });
    }

    static getEstimateTime(req, res, next) {

        let lat = req.query.start_latitude;
        let lnt = req.query.start_longitude;

        request({
            url: 'https://api.uber.com/v1.2/estimates/time?start_latitude='+lat+'&start_longitude='+ lnt,
            headers: {
                'Authorization': server_token,
                'Accept-Language': 'en_US',
                'Content-Type': 'application/json'
            }
        }, (err, response, body) => {
            if (!err && response.statusCode == 200) {
                let content = JSON.parse(body);
                res.send(content.times);
            } else {
                next(err);
            }
        });

    }

    static getEstimatePrice(req, res, next) {

        let start_lat = req.query.start_latitude;
        let start_lnt = req.query.start_longitude;
        let end_lat = req.query.end_latitude;
        let end_lnt = req.query.end_longitude;

        request({
            url: 'https://api.uber.com/v1.2/estimates/price?start_latitude=' +
            start_lat+'&start_longitude=' +
            start_lnt + '&end_latitude=' +
            end_lat + '&end_longitude=' + end_lnt,
            headers: {
                'Authorization': server_token,
                'Accept-Language': 'en_US',
                'Content-Type': 'application/json'
            }
        }, (err, response, body) => {
            console.log(response.statusCode);
            if (!err && response.statusCode == 200) {
                let content = JSON.parse(body);
                res.send(content.prices);
            } else if (!err && response.statusCode != 200) {
                res.status(response.statusCode).send('No result!');
            } else {
                next(err);
            }
        });
    }
}

module.exports = HospitalController;
