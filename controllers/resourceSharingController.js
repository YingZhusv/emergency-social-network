const ShareableResource = require('../models/ShareableResource');

class ResourceSharingController {

    static getAllShareableResources(req, res, next) {
        ShareableResource.getAll().then((shareableResources) => {
            res.json(shareableResources);
        }).catch((err) => {
            next(err);
        });
    }

    static getShareableResourcesByUser(req, res, next) {
        const username = req.params.username;
        ShareableResource.getByUser(username).then((shareableResources) => {
            res.json(shareableResources);
        }).catch((err) => {
            next(err);
        });
    }

    static postShareableResource(req, res, next) {
        const username = req.params.username;
        if (req.user.username == username) {
            ShareableResource.post({
                username: username,
                types: req.body.types,
                description: req.body.description,
                latitude: req.body.latitude,
                longitude: req.body.longitude
            }).then((shareableResource) => {
                res.json(shareableResource);
            }).catch((err) => {
                if (err.message === 'Invalid shareable resource types' ||
                    err.message === 'The length of description should not exceed 70 characters') {
                    res.status(400).send(err.message);
                } else {
                    next(err);
                }
            });
        } else {
            res.status(403).send('Forbidden');
        }
    }
}

module.exports = ResourceSharingController;
