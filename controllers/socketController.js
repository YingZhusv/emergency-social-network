const onlineStates = require('../helpers/UserOnlineStates');

class SocketController {

    static init(server, sessionMiddleware) {
        // Mount Socket.io on the server
        var io = require('socket.io')(server);

        // Configure handlers for client socket events
        io.use(function(socket, next){
            // Wrap the express middleware
            sessionMiddleware(socket.request, {}, next);
        });

        io.on('connection', function(socket) {
            console.log('Socket ' + socket.id + ' connected');

            // Disconnect with invalid socket without session or passport data
            if (!socket.request.session || !socket.request.session.passport) {
                console.log('Invalid socket ' + socket.id);
                socket.disconnect();
                return;
            }

            var username = socket.request.session.passport.user;
            onlineStates.addSocket(username, socket);

            socket.on('join', function(room) {
                console.log('Socket ' + socket.id + ' joining room ' + room);
                socket.join(room);
            });

            socket.on('leave', function(room) {
                console.log('Socket ' + socket.id + ' leaving room ' + room);
                socket.leave(room);
            });

            socket.on('disconnect', function() {
                console.log('Socket ' + socket.id + ' disconnected');
                onlineStates.removeSocket(username, socket);
            });

            socket.on('emitToAll', function(msg) {
                io.in(msg.roomName).emit(msg.eventName, msg.msg);
            });
        });


        SocketController.io = io;
    }
}

module.exports = SocketController;
