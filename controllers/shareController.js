const Message = require('../models/Message');
const User = require('../models/User');
const Twit = require('twit');

class ShareController {
    static postShare(req, res, next) {
        let username = req.user.username;
        let tweet = req.body.content;

        Message.postShare(username, tweet)
            .then(function(message) {
                if (message) {
                    res.status(201).json(message.dataValues);
                }
            }).catch(function (err) {
            console.log(err);
            next(err);
        });

        User.getUser(req.user.username)
            .then(function (user) {
                var T = new Twit({
                    consumer_key:         'OfjrlajsN0wCyxqrs6wq2JDlF',
                    consumer_secret:      'BwaJMF6oGprkWEy8XQCRHPlaQuOSonGqfLUSScVLczSY5dJduW',
                    access_token:          user.twitterToken,
                    access_token_secret:   user.twitterTokenSecret
                });
                T.post('statuses/update', { status: tweet}, function(err, data, response) {
                    if (err) {
                        console.log(err);
                    }
                });
            });
    }

    static getShares(req, res, next) {
        Message.getShares()
            .then(function(messages) {
                res.json(messages);
            });
    }
}

module.exports = ShareController;


