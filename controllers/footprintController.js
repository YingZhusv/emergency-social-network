const socket = require('./socketController');
const Footprint = require('../models/Footprint');
const User = require('../models/User');

class FootprintController {
    static getFootprints(req, res, next) {
        let username = req.params.username;

        User.findOneUser(username)
        .then(function(user){
            if(!user) {
                res.status(404).send("User does not exist");
                return null;
            }
            if(user.shareFootprintsSetting === 0) {
                res.status(403).send("User has disabled this feature");
                return null;
            }

            return Footprint.getFootprints(username);
        })
        .then(function(footprints){
            if(footprints) {
                res.json(footprints);
            }
        });
    }
}

module.exports = FootprintController;
