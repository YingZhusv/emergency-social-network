const Message = require('../models/Message');

const socket = require('./socketController');
const onlineStates = require('../helpers/UserOnlineStates');

class PrivateChatController {
    static postMessage(req, res, next) {
        // Configure location object
        let location = null;
        let username = req.user.username;
        let receiver = req.body.receiver;
        let content = req.body.content;
        if ('latitude' in req.body && 'longitude' in req.body) {
            location = {
                type: 'POINT',
                coordinates: [req.body.longitude, req.body.latitude]
            };
        }

        Message.postPrivateMessage(username, receiver, content, location)
        .then(function(message) {
            if (message) {
                var authorSocket = onlineStates.getSocket(message.author, req.body.socketId);

                var socketMessageBuilder = authorSocket !== null ? authorSocket : socket.io;
                socketMessageBuilder.in("@" + message.author).emit("newMessage", message);
                socketMessageBuilder.in("@" + message.receiver).emit("newMessage", message);

                res.status(201).json(message.dataValues);
            }
        }).catch(function (err) {
            //console.log(err);
            if(err.name == "SequelizeForeignKeyConstraintError") {
                res.status(403).send("One of the users does not exist");
            } else {
                next(err);
            }
        });
    }

    static getMessages(req, res, next) {
        var username1 = req.params.username1;
        var username2 = req.params.username2;

        var curUsername = req.user.username;
        if(curUsername != username1 && curUsername != username2) {
            res.status(403).send("Forbidden");
            return;
        }

        Message.getPrivateMessage(username1, username2)
            .then(function(messages) {
                res.json(messages);
            });
    }

}

module.exports = PrivateChatController;
