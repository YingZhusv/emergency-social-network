const User = require('../models/User');
const StatusCrumb = require('../models/StatusCrumb');

class UserController {

    static getAllUsers(req, res, next) {
        User.getAllUsers()
        .then(function(users) {
            var output = [];
            for(var i = 0; i < users.length; i++) {
                output.push(users[i].safeOutput());
            }
            res.json(output);
        });
    }

    static getNearbyUsers(req, res, next) {
        let longitude = req.query.longitude;
        let latitude = req.query.latitude;
        let distance = req.query.distance;

        var p = 0.017453292519943295;    // Math.PI / 180
        var c = Math.cos;

        User.getAllUsers()
        .then(function(users) {
            var output = [];
            for(var i = 0; i < users.length; i++) {
                if(users[i].latitude === null || users[i].longitude === null) {
                    continue;
                }
                var a = 0.5 - c((latitude - users[i].latitude) * p)/2 +
                      c(users[i].latitude * p) * c(latitude * p) *
                      (1 - c((longitude - users[i].longitude) * p))/2;
                if(12742 * Math.asin(Math.sqrt(a))<distance) {
                    output.push(users[i].safeOutput());
                }
            }
            res.json(output);
        });
    }

    static getUser(req, res, next) {
        let username = req.params.username;

        User.getUser(username)
        .then(function(user) {
            if (user) {
                res.json(user.safeOutput());
            } else {
                res.status(404).send("Not found");
            }
        });
    }

    static postStatus(req, res, next) {
        const username = req.params.username;
        const statusCode = req.body.statusCode;
        const locationDesc = req.body.locationDesc;

        let location = null;
        if ('latitude' in req.body && 'longitude' in req.body) {
            location = {
                type: 'POINT',
                coordinates: [req.body.longitude, req.body.latitude]
            };
        }

        if (req.user.username == username) {
            User.findOneUser(username)
                .then(function(user){
                    if (user) {
                        return user.updateStatusCode(statusCode);
                    }
                    res.status(403).send("Forbidden");
                }).then(function(user) {
                    return user.updateLocation(location);
                }).then(function() {
                    return StatusCrumb.createStatusCrumb(username, statusCode, location, locationDesc);
                }).then(function(crumb) {
                    res.status(201).json(crumb.dataValues);
                }).catch(function (err) {
                    console.log(err);
                    next(err);
                });
        } else {
            res.status(403).send("Forbidden");
        }
    }

    static getStatuses(req, res, next) {
        var username = req.params.username;

        StatusCrumb.getStatuses(username)
            .then(function(crumbs) {
                res.json(crumbs);
            });
    }

    static updateShareFootprintsSetting(req, res, next) {
        var username = req.user.username;
        var newSetting = parseInt(req.body.setting);

        User.updateShareFootprintsSetting(username, newSetting)
        .then(function(users) {
            res.json(newSetting);
        });
    }
}

module.exports = UserController;
