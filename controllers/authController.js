const User = require('../models/User');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, function(username, password, done) {
        User.findOneUser(username)
            .then(function(user) {
                if (!user) {
                    return done(null, false, { code: 404, message: 'User not found' });
                } else if(!user.isActive) {
                    return done(null, false, { code: 401, message: 'Account deactivated.'});
                }
                user.validatePassword(password, function(valid) {
                    if(valid) {
                        return done(null, user);
                    } else {
                        return done(null, false, { code: 401, message: 'Incorrect password.'});
                    }
                });
            });
    }
));

passport.use(new TwitterStrategy({
        consumerKey: 'OfjrlajsN0wCyxqrs6wq2JDlF',
        consumerSecret: 'BwaJMF6oGprkWEy8XQCRHPlaQuOSonGqfLUSScVLczSY5dJduW',
        callbackURL: process.env.CALL_BACK_URL + '/api/users/twitter/callback',
        passReqToCallback: true
    },
    function(req, token, tokenSecret, profile, done) {
        User.updateTwitter(req.user.username, token, tokenSecret);
        done(null, req.user);
    }));

passport.serializeUser(function(user, done) {
    done(null, user.username);
});

passport.deserializeUser(function(username, done) {
    User.findOneUser(username)
        .then(function(user) {
            done(null, user);
        }).catch(function (err) {
        done(err, null);
    });
});

function malformedUsernameAndPassword(username, password) {
    if(User.isUsernameReserved(username)) {
        return "Username is reserved";
    } else if(!User.isUsernameLengthSufficient(username)) {
        return "Username is too short";
    } else if(!User.isPasswordValid(password)) {
        return "Password is too weak";
    } else if(!User.isUsernameAlphanumeric(username)) {
        return "Username can only contain a-z, A-Z, 0-9, and symbols ._-";
    }
    return null;
}

function passportLogin(req, res, next, successCode) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {
            return res.status(info.code).send(info.message);
        }
        req.logIn(user, function(err) {
            if (err) { return next(err); }

            user.updateLastLoginAt();
            return res.status(successCode).json(user.safeOutput());
        });
    })(req, res, next);
}

function passportShare(req, res, next) {
    passport.authenticate('twitter', function(err) {
        if (err) { return next(err); }
    })(req, res, next);
}

function passportCallback(req, res, next) {
    passport.authenticate('twitter', { successRedirect: '/#!/share',
                                        failureRedirect: '/' })(req, res, next);
}

class AuthController {
    static signup(req, res, next) {
        var username = req.body.username;
        var password = req.body.password;

        var formatErr = malformedUsernameAndPassword(username, password);
        if(formatErr !== null) {
            res.status(400).send(formatErr);
            return;
        }

        User.createUser(username, password)
            .then(function(createdUser) {
                if(createdUser) {
                    passportLogin(req, res, next, 201);
                } else {
                    res.status(500).send("Cannot create user");
                }
            }).catch(function (err) {
                res.redirect(307, "/api/users/login");
            });
    }

    static login(req, res, next) {
        var username = req.body.username;
        var password = req.body.password;

        var formatErr = malformedUsernameAndPassword(username, password);
        if(formatErr !== null) {
            res.status(400).send(formatErr);
            return;
        }

        passportLogin(req, res, next, 200);
    }

    static share(req, res, next) {
        passportShare(req, res, next, 200);
    }

    static callback(req, res, next) {
        passportCallback(req, res, next, 200);
    }

    static logout(req, res, next) {
        if(!req.user) {
            res.status(400).send();
            return;
        }

        req.logout();
        res.status(200).send();
    }

    static authenticate(req, res, next) {
        if(!req.user) {
            res.status(403).send("Forbidden");
            return;
        }

        if(!req.user.isActive) {
            req.logout();
            res.redirect(307, "/");
            return;
        }

        next();
    }

    static authenticateAdmin(req, res, next) {
        if(!req.user || req.user.privilegeLevel < 2) {
            res.status(403).send("Forbidden. Needs admin privilege.");
            return;
        }

        next();
    }

    static authenticateCoordinator(req, res, next) {
        if(!req.user || req.user.privilegeLevel < 1) {
            res.status(403).send("Forbidden. Needs coordinator privilege.");
            return;
        }

        next();
    }
}

module.exports = AuthController;
