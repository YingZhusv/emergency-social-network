const User = require('../models/User');
const StatusCrumb = require('../models/StatusCrumb');
const onlineStates = require('../helpers/UserOnlineStates');

function sendForceLogout(username, msg) {
    var sockets = onlineStates.getSocketsForUser(username);
    for (var key in sockets) {
        if (sockets.hasOwnProperty(key)) {
            sockets[key].emit("forceLogout", msg);
        }
    }
}

class AdminController {
    static activateUser(req, res, next) {
        let username = req.params.username;

        User.updateIsActive(username, true)
        .then(function(result) {
            if(result) {
                res.json(result.safeOutput());
            } else {
                res.status(404).send("User not found");
            }
        });
    }

    static deactivateUser(req, res, next) {
        let username = req.params.username;

        User.updateIsActive(username, false)
        .then(function(result) {
            if(result) {
                res.json(result.safeOutput());
                // logout

                sendForceLogout(username, "Account deactivated.");
            } else {
                res.status(404).send("User not found");
            }
        }).catch(function(err) {
            res.status(400).send(err.message);
        });
    }

    static updateUser(req, res, next) {
        let username = req.params.username;

        User.updateUser(username, req.body)
        .then(function(result) {
            if(result) {
                if(username !== result.username) {
                    sendForceLogout(username, "Username modified.");
                }

                res.json(result.safeOutput());
            } else {
                res.status(400).send("User not found");
            }
        }).catch(function(err){
            res.status(400).send(err.message);
            console.log(err);
        });
    }

    static getAllUsers(req, res, next) {
        User.getAllUsers(true)
            .then(users => {
                res.json(users.map(user => user.safeOutput()));
            });
    }
}

module.exports = AdminController;
