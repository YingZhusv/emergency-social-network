module.exports.connections = {};

module.exports.addSocket = function(username, socket) {
    if(!module.exports.connections[username]) {
        module.exports.connections[username] = {};
    }

    module.exports.connections[username][socket.id] = socket;
};

module.exports.removeSocket = function(username, socket) {
    if(!module.exports.connections[username] || socket === null) {
        return;
    }

    delete module.exports.connections[username][socket.id];
};

module.exports.getSocketsForUser = function(username) {
    if(!module.exports.connections[username]) {
        return null;
    }

    return module.exports.connections[username];
};

module.exports.getSocket = function(username, socketId) {
    if(!module.exports.connections[username] || socketId === null) {
        return null;
    }

    return module.exports.connections[username][socketId];
};

module.exports.isOnline = function(username) {
    return module.exports.connections[username] && Object.keys(module.exports.connections[username]).length > 0;
};
