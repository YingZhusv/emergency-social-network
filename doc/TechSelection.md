# Technology

## Front-Tier
1. Angular 1.X
2. Bootstrap

## Back-Tier
1. Node.js
2. Express.js
3. Passport.js
4. Socket.io
5. Sequelize.js

## Database
1. PostgreSQL

## Package Manager
1. npm
2. bower