(function() {

angular
    .module('FseEsnApp')
    .controller('ProfileController', ProfileController);

ProfileController.$inject = [
    '$scope',
    '$rootScope',
    'StaticDataService',
    'UserService'
];
function ProfileController($scope, $rootScope, StaticDataService, UserService) {

    var selectedStatusIndex = -1;
    init();
    if ($rootScope.username) {
        getStatusAndPopulate();
        getShareFootprintsSetting();
    }

    function init() {
        $scope.selectedRow = null;

        $scope.tempDistance = $rootScope.distance;
        $scope.distances = [1, 5, 10];

        $scope.shareFootprintsSetting = "";

        /////// for hosptialWarning Message ////
        $rootScope.statusCrumbsNotFound = false;
        ////////////////////////////////////////

        $scope.statuses = StaticDataService.getStatusList();
    }

    function getStatusAndPopulate() {
        UserService.getStatuses($rootScope.username)
            .then((response) => {
                if (response.data.length > 0) {
                    const latestStatusCrumb = response.data[0];
                    $scope.locationDescription = latestStatusCrumb.locationDesc;
                    for (let i = 0; i < 3; ++i) {
                        if (latestStatusCrumb.statusCode == $scope.statuses[i].colorCode) {
                            $scope.setClickedRow(i);
                            break;
                        }
                    }
                }
            });
    }

    function getShareFootprintsSetting() {
        UserService.getUser($rootScope.username).then((response) => {
            if(response.data) {
                $scope.shareFootprintsSetting = response.data.shareFootprintsSetting.toString();
            }
        });
    }

    $scope.selectDistance = function(index) {
        $scope.tempDistance = $scope.distances[index];
    };
    $scope.setClickedRow = function(index) {
        $scope.selectedRow = index;
        selectedStatusIndex = index;
    };
    $scope.closeStatusWarning = function() {
        $scope.showStatusWarning = 0;
    };
    $scope.closeStatusSuccess = function() {
        $scope.showStatusSuccess = 0;
    };
    $scope.saveStatusGoToUserList = function() {
        if (selectedStatusIndex >= 0 && selectedStatusIndex <= 2) {
            if ($rootScope.username) {
                // Pack status crumb data
                const statusCrumb = {
                    statusCode: $scope.statuses[selectedStatusIndex].colorCode,
                    locationDesc: $scope.locationDescription
                };
                if ($rootScope.geolocationCoords !== null) {
                    statusCrumb.latitude = $rootScope.geolocationCoords.latitude;
                    statusCrumb.longitude = $rootScope.geolocationCoords.longitude;
                }

                // Post status crumb to server
                UserService.postStatus($rootScope.username, statusCrumb)
                    .then((response) => {
                        return UserService.updateShareFootprintSetting($rootScope.username, $scope.shareFootprintsSetting);
                    }).then((response) => {
                        $scope.showStatusSuccess = 1;
                        $rootScope.locationAutoSharingEnabled = $scope.shareFootprintsSetting === '2';
                    })
                    .catch((error) => {
                        console.log(error);
                    });

                $rootScope.distance = $scope.tempDistance;
            }
        } else {
            $scope.showStatusWarning = 1;
        }
    };
}

})();
