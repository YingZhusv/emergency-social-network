(function() {

angular
    .module('FseEsnApp')
    .directive('scrollBottom', ScrollBottomDirective)
    .controller('ChatController', ChatController);

ChatController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$location',
    'ChatService',
    'UserService',
    'SearchFilterService'
];
function ChatController($scope, $rootScope, $stateParams, $location, ChatService, UserService, SearchFilterService) {
    // Initialize the dropdown user list
    init();

    //  Local function definitions --------------------------------------------------
    function init() {
        resetParameters();
        initUIEventHandlers();
        initSocket();
        initUserList();
        setupWatchGeoLocaton();
        setupWatchMsgLength();
        setupSearchMsg();
    }

    function resetParameters() {
        // set $scope.userIndex to uninitialized state
        $scope.userIndex = -2;

        $scope.msgs = [];
        $scope.finalSearchMsg = '';
        $scope.totalMsgNumber = 0;
        $scope.msgNumberUpperBound = 10;
        $scope.msgLeft = 0;
        $scope.searchMsg = '';
        $scope.numberOfNearby = 0;

        /////// for hosptialWarning Message ////
        $rootScope.statusCrumbsNotFound = false;
        ////////////////////////////////////////

        $scope.isSearchShown = false;
    }

    function initUIEventHandlers() {
        // Set up UI event handlers
        $scope.selectUser = selectUser;
        $scope.sendChatContent = sendChatContent;
        $scope.calcTotalUnreadCount = calcTotalUnreadCount;
    }

    function initSocket() {
        // Configure socket event listeners
        ChatService.onNewMessage(pushNewPublicMessage);
        ChatService.onNewMessage(pushNewPrivateMessage);
        $scope.$on("$destroy", () => {
            ChatService.removeNewMessageListener(pushNewPublicMessage);
            ChatService.removeNewMessageListener(pushNewPrivateMessage);
        });
    }

    function initUserList() {
        $scope.users = [];
        $scope.privateUnreadCounters = {};
        UserService.getAllUsers().then((response) => {
            $scope.users = response.data;

            $scope.publicUnreadCounter = 0;
            for (let i = 0; i < $scope.users.length; ++i) {
                $scope.privateUnreadCounters[$scope.users[i].username] = 0;
            }

            preseletUserFromStateParams();
        }).catch((error) => {
            console.log(error);
        });
    }

    function preseletUserFromStateParams() {
        if($stateParams.id) {
            for(var i = 0; i < $scope.users.length; i += 1) {
                if($stateParams.id == $scope.users[i].username) {
                    selectUser(i);
                    break;
                }
            }
        } else {
            selectUser(-1);
        }
    }

    function selectUser(userIndex) {
        if ($scope.userIndex !== userIndex) {
            $scope.userIndex = userIndex;
            if (userIndex === -1) {
                joinPublicChat();
            } else {
                joinPrivateChat($scope.users[userIndex].username);
            }
        }
    }

    function setupWatchGeoLocaton() {
        $rootScope.$watchGroup(['geolocationCoords.latitude', 'geolocationCoords.longitude'], function() {
            if($rootScope.geolocationCoords !== null) {

                var latitude = $rootScope.geolocationCoords.latitude;
                var longitude = $rootScope.geolocationCoords.longitude;

                UserService.getNearbyUsers(longitude, latitude, $rootScope.distance)
                    .then((response) => {
                        $rootScope.nearByUsers = UserService.removeSpecificUser(response.data, $rootScope.username);
                        $scope.numberOfNearby = $rootScope.nearByUsers.length;
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }
        });
    }

    function setupWatchMsgLength() {
        $scope.$watch('msgs.length', function() {
            $scope.totalMsgNumber = $scope.msgs.length;
            if($scope.finalSearchMsg === '') {
                $scope.msgNumberUpperBound = $scope.totalMsgNumber;
            }
        });
    }

    function setupSearchMsg() {
        $scope.$watch('searchMsg', function() {
            $scope.finalSearchMsg = SearchFilterService.filterSearchKeywords($scope.searchMsg);
            $scope.msgNumberUpperBound = $scope.finalSearchMsg === '' ? $scope.totalMsgNumber : 10;
            notifyMsgLeftChanged();
        });
    }

    $scope.msgFilter = function(msg) {
        if ($scope.finalSearchMsg === '') {
            if($scope.searchMsg !== '') {
                // searching only stop words
                return false;
            } else {
                //not in searching, show all
                return true;
            }
        }

        return SearchFilterService.messageHasKeywords(msg, $scope.finalSearchMsg);
    };

    $scope.clearSearch = function() {
        $scope.searchMsg = '';
        $scope.msgNumberUpperBound = $scope.totalMsgNumber;
    };

    $scope.getMore = function() {
        $scope.msgNumberUpperBound += 10;
        notifyMsgLeftChanged();
    };

    $scope.toggleSearchForm = function toggleSearchForm() {
        $scope.clearSearch();
        $scope.isSearchShown = !$scope.isSearchShown;
    };

    function notifyMsgLeftChanged() {
        $scope.msgLeft = Math.min(10, $scope.filtered.length - $scope.msgNumberUpperBound);
    }

    function joinPublicChat() {
        $location.search('id', null);
        $scope.messageType = 'PUBLIC';
        $scope.receiver = '';
        $scope.title = '[ Public ]';
        $scope.publicUnreadCounter = 0;

        messageTypeOri = $scope.messageType;
        receiverOri = $scope.receiver;
        chatContent = $scope.chatContent;

        // Load all past chat messages
        $scope.msgs = [];
        ChatService.getPublicMessages().then((response) => {
            // TODO: load on demand (e.g. click/scroll to load more)
            $scope.msgs = response.data;
            $scope.msgNumberUpperBound = $scope.msgs.length;
        }).catch((error) => {
            console.log(error);
        });
    }

    function joinPrivateChat(username) {
        $location.search('id', username);
        $scope.messageType = 'PRIVATE';
        $scope.receiver = username;
        $scope.title = '[ Private ] ' + $scope.receiver;
        $scope.privateUnreadCounters[$scope.receiver] = 0;

        messageTypeOri = $scope.messageType;
        receiverOri = $scope.receiver;
        chatContent = $scope.chatContent;

        // Load all past chat messages
        $scope.msgs = [];
        ChatService.getPrivateMessages($rootScope.username, $scope.receiver).then((response) => {
            // TODO: load on demand (e.g. click/scroll to load more)
            $scope.msgs = response.data;
            $scope.msgNumberUpperBound = $scope.msgs.length;
        }).catch((error) => {
            console.log(error);
        });
    }

    var messageTypeOri = $scope.messageType;
    var receiverOri = $scope.receiver;
    var chatContent = $scope.chatContent;

    $scope.localBroadcast = function () {
        messageTypeOri = $scope.messageType;
        receiverOri = $scope.receiver;
        chatContent = $scope.chatContent;

        $scope.messageType = 'PRIVATE';
        for(var i = 0; i < $rootScope.nearByUsers.length; ++i) {

            $scope.receiver = $rootScope.nearByUsers[i].username;
            sendChatContent(true);
            $scope.chatContent = chatContent;
        }

        $scope.messageType = messageTypeOri;
        $scope.receiver = receiverOri;
        // Clear chat content
        $scope.chatContent = "";
    };

    function sendChatContent(isBroadcast) {
        if ($scope.chatContent && $scope.chatContent.length > 0) {
            // Build a new message package
            var receiverInput = $scope.receiver;
            let message = {
                messageType: $scope.messageType,
                author: $rootScope.username,
                receiver: $scope.receiver,
                content: $scope.chatContent
            };
            if ($rootScope.geolocationCoords !== null) {
                message.latitude = $rootScope.geolocationCoords.latitude;
                message.longitude = $rootScope.geolocationCoords.longitude;
            }
            // Post the newly packed message
            if ($scope.messageType === 'PUBLIC') {
                ChatService.postPublicMessage(message).then((response) => {
                    if(!isBroadcast) {
                        $scope.msgs.push(response.data);
                    }
                }).catch((error) => {
                    console.log(error);
                });
            } else {
                ChatService.postPrivateMessage(message).then((response) => {
                    if(receiverInput == receiverOri && receiverOri !== null && receiverOri !== "") {
                        $scope.msgs.push(response.data);
                    }
                }).catch((error) => {
                    console.log(error);
                });
            }

            // Clear chat content
            $scope.chatContent = "";
        }
    }

    function pushNewPublicMessage(message) {
        if (message.messageType === 'PUBLIC') {
            if (message.messageType === $scope.messageType) {
                $scope.$apply(() => $scope.msgs.push(message));
            } else {
                $scope.$apply(() => ++$scope.publicUnreadCounter);
            }
        }
    }

    function pushNewPrivateMessage(message) {
        if (message.messageType === 'PRIVATE') {
            if (message.messageType === $scope.messageType &&
                ((message.author === $rootScope.username && message.receiver === $scope.receiver) ||
                 (message.receiver === $rootScope.username && message.author === $scope.receiver))) {
                $scope.$apply(() => $scope.msgs.push(message));
            } else {
                var sender = message.receiver === $rootScope.username ? message.author : message.receiver;
                if($scope.privateUnreadCounters.hasOwnProperty(sender)) {
                    $scope.$apply(() => ++$scope.privateUnreadCounters[sender]);
                }
            }
        }
    }

    function calcTotalUnreadCount() {
        let totalUnreadCount = $scope.publicUnreadCounter;
        for (let username in $scope.privateUnreadCounters) {
            if ($scope.privateUnreadCounters.hasOwnProperty(username)) {
                totalUnreadCount += $scope.privateUnreadCounters[username];
            }
        }
        return totalUnreadCount;
    }

}

function ScrollBottomDirective() {
    return {
        scope: {
            scrollBottom: "="
        },
        link(scope, element) {
            scope.$watchCollection('scrollBottom', (newValue) => {
                if (newValue) {
                    $(element).scrollTop($(element)[0].scrollHeight);
                }
            });
        }
    };
}

})();
