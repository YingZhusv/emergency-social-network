(function() {

angular
    .module('FseEsnApp')
    .controller('UserListController', UserListController);

UserListController.$inject = [
    '$scope',
    '$rootScope',
    'UserService'
];
function UserListController($scope, $rootScope, UserService) {

    resetParameters();
    getAllUsers();
    setupGeoLocationWatcher();

    var coords = {
        latitude: null,
        longitude: null
    };

    function resetParameters() {
        $scope.isStatusOkChecked = true;
        $scope.isStatusHelpChecked = true;
        $scope.isStatusEmergencyChecked = true;
        $scope.isStatusUndefinedChecked = true;
        // Clear the list
        $scope.users = [];

        $scope.isSearchShown = false;

        /////// for hosptialWarning Message ////
        $rootScope.statusCrumbsNotFound = false;
        ////////////////////////////////////////
    }

    function getAllUsers() {
        UserService.getAllUsers()
            .then((response) => {
                $scope.users = response.data;
                $scope.users = UserService.addDistanceAttribute($scope.users, coords.latitude, coords.longitude, $rootScope.username);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    function setupGeoLocationWatcher() {
        $rootScope.$watchGroup(['geolocationCoords.latitude', 'geolocationCoords.longitude'], function() {
            if($rootScope.geolocationCoords !== null) {
                coords.latitude = $rootScope.geolocationCoords.latitude;
                coords.longitude = $rootScope.geolocationCoords.longitude;

                $scope.users = UserService.addDistanceAttribute($scope.users, coords.latitude, coords.longitude, $rootScope.username);

                UserService.getNearbyUsers(coords.longitude, coords.latitude, $rootScope.distance)
                    .then((response) => {
                        $rootScope.nearByUsers = response.data;
                        $rootScope.nearByUsers = UserService.modifyNearbyUser($rootScope.nearByUsers, coords.latitude, coords.longitude, $rootScope.username);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }
        });
    }

    $scope.getStatusColor = function(statusCode) {
        return UserService.getStaticStatusColor(statusCode);
    };

    $scope.clearSearch = function() {
        $scope.searchText = '';
    };

    $scope.setColor = function(isOnline) {
        return {
            color: (isOnline) ? "black" : "grey"
        };
    };

    $scope.toggleSearchForm = function toggleSearchForm() {
        $scope.clearSearch();
        $scope.isSearchShown = !$scope.isSearchShown;
    };
}

})();
