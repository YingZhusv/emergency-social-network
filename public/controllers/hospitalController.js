const MAX_BUTTON_NUM = 3;

(function() {

angular
    .module('FseEsnApp')
    .controller('HospitalController', HospitalController);

HospitalController.$inject = [
    '$scope',
    'HospitalService',
    '$rootScope',
    '$q',
    '$location'
];

function HospitalController($scope, HospitalService, $rootScope, $q, $location) {
    $scope.dataLoaded = false;

    $rootScope.$watchGroup(['geolocationCoords.latitude', 'geolocationCoords.longitude'], function() {
        if ($rootScope.geolocationCoords !== null) {

            $rootScope.statusCrumbsNotFound = false;
            console.log($rootScope.geolocationCoords);

            let user_lat = $rootScope.geolocationCoords.latitude;
            let user_lnt = $rootScope.geolocationCoords.longitude;
            let promise_array = [];
            let data;
            let price = {};

            let nearbyPromise = HospitalService.getHospitalNearby(user_lat, user_lnt)
                .then((response) => {
                    console.log(response);
                    data = processMapData(response.data);

            });
            promise_array.push(nearbyPromise);

            let estimatePromise = HospitalService.getHospitalEstimateTime(user_lat, user_lnt)
                .then((response) => {
                    for (let object of response.data) {
                        if (object.localized_display_name == 'uberX') {
                            $scope.time = Math.round(object.estimate / 60);
                            break;
                        }
                    }
                });
            promise_array.push(estimatePromise);

            $q.all(promise_array).then(() => {
                let promise_array_2 = [];

                for (let i = 0;i<data.length;i++) {
                    let promise = HospitalService.getHospitalEstimatePrice(user_lat, user_lnt, data[i].lat, data[i].lng);
                    promise_array_2.push(promise);
                }
                $q.all(promise_array_2).then((response) => {
                    data = processResponseData(response, data);
                    console.log(data);
                    $scope.mapDataArray = data;
                    $scope.dataLoaded = true;
                });

            });
        } else {
            console.log('user location not found!');
            $rootScope.statusCrumbsNotFound = true;
            $location.path('/hospitalInstruction');
        }
    });

    $scope.cancel = function() {
        $location.path('/hospitalInstruction');
    };

}

function processMapData(data) {
    let mapDataArray = [];
    for (let i=0;i<data.length && i<MAX_BUTTON_NUM;i++) {
        let lat = data[i].geometry.location.lat;
        let lng = data[i].geometry.location.lng;
        let name = data[i].name;
        let link = 'https://m.uber.com/ul/?action=setPickup&client_id=%20hUsEBEOyhLkFK6W4Jj6fNkY2sgHbrCix&pickup=my_location&dropoff[latitude]='+lat+'&dropoff[longitude]='+lng;
        let mapData = { 'name': name, 'link': link, 'lat' : lat, 'lng' : lng};

        mapDataArray.push(mapData);

    }
    return mapDataArray;
}

function processResponseData(response, data) {
    for (let i = 0;i<data.length;i++) {
        for (let j = 0;j<response[i].data.length;j++) {
            if (response[i].data[j].display_name === 'uberX') {
                data[i].price = response[i].data[j].estimate + response[i].data[j].currency_code;
                break;
            }
        }
    }
    return data;
}


})();
