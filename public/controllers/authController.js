(function() {

angular
    .module('FseEsnApp')
    .controller('AuthController', AuthController);

AuthController.$inject = [
    '$scope',
    '$rootScope',
    '$location',
    'AuthService'
];
function AuthController($scope, $rootScope, $location, AuthService) {
    $scope.showError = 0;

    $scope.signUp = function() {
        AuthService.signUp({
            username: $scope.username,
            password: $scope.password
        }).then((response) => {
            $location.path('/instruction');
            $scope.showError = 0;
            $rootScope.username = response.data.username;
        }).catch((error) => {
            console.log(error);
        });
    };

    $scope.logIn = function() {
        AuthService.logIn({
            username: $scope.username,
            password: $scope.password
        }).then((response) => {
            window.location.href = '/';
        }).catch((error) => {
            $scope.showError = error.status;
            if(error.data) {
                $scope.data = error.data;
            }
        });
    };

    $scope.logOut = function() {
        AuthService.logOut()
            .then((response) => {
                window.location.href = "/";
            }).catch((error) => {
                console.log(error);
            });
    };

    $scope.reset = function() {
        $scope.showError = 0;
        $scope.username = "";
        $scope.password = "";
    };
}

})();
