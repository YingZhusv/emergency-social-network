(function() {

angular
    .module('FseEsnApp')
    .controller('HospitalInstructionController', HospitalInstructionController);

HospitalInstructionController.$inject = [
    '$scope',
    '$location',
];
function HospitalInstructionController($scope, $location) {

    $scope.start = function() {
        $location.path('/hospital');
    };
}

})();
