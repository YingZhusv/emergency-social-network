(function() {

angular
    .module('FseEsnApp')
    .controller('AdminController', AdminController);

AdminController.$inject = [
    '$scope',
    '$rootScope',
    'AdminService'
];
function AdminController($scope, $rootScope, AdminService) {

    init();

    function init() {
        $scope.users = [];
        AdminService.getAllUsers().then((response) => {
            $scope.users = response.data;
            $scope.selectedUserIndex = 0;

            $scope.userActiveState = ($scope.users[$scope.selectedUserIndex].isActive)? "Active" : "Inactive";

            $scope.userPrivilegeLevel = codeToPrivilegeLevel($scope.users[$scope.selectedUserIndex].privilegeLevel);
        }).catch((error) => {
            console.log(error);
            window.location.href = "/";
        });
    }
    $scope.selectUser = function (userIndex) {
        $scope.selectedUserIndex = userIndex;
        $scope.userActiveState = ($scope.users[userIndex].isActive)? "Active" : "Inactive";
        $scope.userPrivilegeLevel = codeToPrivilegeLevel($scope.users[userIndex].privilegeLevel);
        $scope.showStatusSuccess = 0;
        $scope.showStatusWarning = 0;
    };

    $scope.changeAccountStatus = function (index) {
        if(index === 0) {
            $scope.userActiveState = "Active";
        } else {
            $scope.userActiveState = "Inactive";
        }
        $scope.showStatusSuccess = 0;
        $scope.showStatusWarning = 0;
    };

    $scope.changePrivilegeLevel = function (index) {
        $scope.users[$scope.selectedUserIndex].privilegeLevel = index;
        $scope.userPrivilegeLevel = codeToPrivilegeLevel($scope.users[$scope.selectedUserIndex].privilegeLevel);
        $scope.showStatusSuccess = 0;
        $scope.showStatusWarning = 0;
    };

    $scope.closesSuccessInfo = function() {
        $scope.showStatusSuccess = 0;
    };
    $scope.closesSuccessInfo = function() {
        $scope.showStatusWarning = 0;
    };

    $scope.saveAdminSetting = function () {
        if($scope.userActiveState === "Active") {
            AdminService.activateUser($scope.users[$scope.selectedUserIndex].username);
        } else {
            AdminService.deactivateUser($scope.users[$scope.selectedUserIndex].username)
            .catch((error) => {
                $scope.showStatusWarning = 1;
                $scope.errorMsg = error.data;
            });
        }
        var newUser = {
            privilegeLevel: $scope.users[$scope.selectedUserIndex].privilegeLevel
        };
        if($scope.newUsername !== null && $scope.newUsername !== '') {
            newUser.username = $scope.newUsername;
        }
        if($scope.newPassword !== null && $scope.newPassword !== '') {
            newUser.password = $scope.newPassword;
        }
        AdminService.updateUser($scope.users[$scope.selectedUserIndex].username, newUser)
            .then(function () {
                init();
                $scope.showStatusSuccess = 1;
            })
            .catch((error) => {
                init();
                $scope.showStatusWarning = 1;
                $scope.errorMsg = error.data;
            });
        $scope.newUsername = '';
        $scope.newPassword = '';
    };

    function codeToPrivilegeLevel (code) {
        switch(code) {
            case 0:
                return "Citizen";
            case 1:
                return "Coordinator";
            case 2:
                return "Administrator";
        }
    }
}

})();
