(function() {

angular
    .module('FseEsnApp')
    .controller('HomeController', HomeController);

HomeController.$inject = [
    '$scope',
    '$location',
    '$rootScope'
];
function HomeController($scope, $location, $rootScope) {
    /////// for hosptialWarning Message ////
    $rootScope.statusCrumbsNotFound = false;
    ////////////////////////////////////////

    $scope.join = function() {
        $location.path('/login');
    };
}

})();
