(function() {

angular
    .module('FseEsnApp')
    .controller('FootprintsController', FootprintsController);

FootprintsController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    'FootprintsService',
    'NgMap',
    'SocketService'
];

function FootprintsController($scope, $rootScope, $stateParams, FootprintsService, NgMap, SocketService) {

    var map;
    var markers = [];
    var curMarker = null;


    NgMap.getMap().then(function(m) {
        map = m;
        return FootprintsService.getFootprints($stateParams.username);
    }).then((response) => {
        var data = response.data;

        populateMap(data);
        setupSocket();

        $scope.$on("$destroy", destroy);
    });

    function setupSocket() {
        SocketService.joinRoom('#location@' + $stateParams.username);
        SocketService.onEvent('locationChanged', locationUpdatedCallback);
    }

    function destroy() {
        clearMarkers();
        if(curMarker) {
            curMarker.setMap(null);
        }

        SocketService.leaveRoom('#location@' + $stateParams.username);
        removeEventListener('locationChanged', locationUpdatedCallback);
    }

    function locationUpdatedCallback(location) {
        if(curMarker) {
            curMarker.setMap(null);
        }

        curMarker = generateMarkerForDatum({
            type: 'Current Location',
            location: location,
            time: new Date()
        });
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    function generateWindowContentForDatum(datum) {
        return "<h1>" + generateTitleForDatum(datum) + "</h1>" +
               "<div>Time: " + datum.time + "</div>" +
               "<div>location: (" + datum.location[1] + ", " + datum.location[0] + ")</div>";
    }

    function generateTitleForDatum(datum) {
        return datum.type;
    }

    function getPtFromDatum(datum) {
        return new google.maps.LatLng(datum.location[1], datum.location[0]);
    }

    function getMarkerIconForDatum(datum) {
        switch(datum.type) {
            case 'PRIVATE Message':
                return '/img/ic_message_black_24dp_1x.png';
            case 'PUBLIC Message':
                return '/img/ic_public_black_24dp_1x.png';
            case 'Current Location':
                return '/img/ic_my_location_red_24dp_1x.png';
            default:
                return '/img/ic_location_on_black_24dp_1x.png';
        }
    }

    function generateMarkerForDatum(datum) {
        var pt = getPtFromDatum(datum);
        var title = generateTitleForDatum(datum);

        var infowindow = new google.maps.InfoWindow({
            content: generateWindowContentForDatum(datum)
        });

        var marker = new google.maps.Marker({
            position: pt,
            map: map,
            title: title,
            icon: getMarkerIconForDatum(datum)
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

        return marker;
    }

    function addMarkersForData(data) {
        var bounds = new google.maps.LatLngBounds();

        for(var i = Math.min(data.length, 10) - 1; i >= 0; i -= 1) {
            var datum = data[i];
            var marker = generateMarkerForDatum(datum);
            markers.push(marker);

            if(i > 0) {
                var line = new google.maps.Polyline({
                    path: [{lat: data[i].location[1], lng: data[i].location[0]},
                           {lat: data[i - 1].location[1], lng: data[i - 1].location[0]}],
                    icons: [{
                        icon: {
                          path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '100%'
                    }],
                    map: map
                });
                markers.push(line);
            }

            bounds.extend(getPtFromDatum(datum));
        }

        map.fitBounds(bounds);
    }

    function populateMap(data) {
        clearMarkers();
        addMarkersForData(data);
    }


}

})();
