(function() {

    angular
        .module('FseEsnApp')
        .controller('ShareController', ShareController);

    ShareController.$inject = [
        '$scope',
        '$rootScope',
        'ShareService'
    ];

    function ShareController($scope, $rootScope, ShareService) {
        // Clear the message list
        $scope.msgs = [];

        // Retreive all past shares
        ShareService.getShares()
            .then((response) => {
                // TODO: load on demand (e.g. click/scroll to load more)
                $scope.msgs = response.data;
            })
            .catch((error) => {
                // TODO: display error
                console.log(error);
            });

        $scope.sendShareContent = function() {
            if ($scope.shareContent && $scope.shareContent.length > 0) {
                ShareService.getUserInformation($rootScope.username).then((response) => {
                    console.log(response);
                    let user = response.data;
                    if (user.twitterToken !== null && user.twitterTokenSecret !== null) {
                        let share = {
                            messageType: 'SHARE',
                            author: $rootScope.username,
                            content: $scope.shareContent
                        };

                        // Post a new shared message
                        ShareService.postShare(share)
                            .then((response) => {
                                // TODO: mark as "success"
                                $scope.msgs.unshift(response.data); // unshift = push front
                            })
                            .catch((error) => {
                                // TODO: display error
                                console.log(error);
                            });
                        $scope.shareContent = "";
                    }
                });
            }
        };
    }

})();


