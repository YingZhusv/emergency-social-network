(function() {

angular
    .module('FseEsnApp')
    .controller('InstructionController', InstructionController);

InstructionController.$inject = [
    '$scope',
    'StaticDataService',
    '$rootScope'
];
function InstructionController($scope, StaticDataService, $rootScope) {
    $scope.statuses = StaticDataService.getStatusList();

    /////// for hosptialWarning Message ////
    $rootScope.statusCrumbsNotFound = false;
    ////////////////////////////////////////

    $scope.goToUserList = function() {
        window.location.href = '/';
    };
}

})();
