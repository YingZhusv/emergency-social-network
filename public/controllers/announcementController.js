(function() {

angular
    .module('FseEsnApp')
    .directive('scrollTop', ScrollTopDirective)
    .controller('AnnouncementController', AnnouncementController);

AnnouncementController.$inject = [
    '$scope',
    '$rootScope',
    'AnnouncementService',
    'SearchFilterService',
    'UserService'
];
function AnnouncementController($scope, $rootScope, AnnouncementService, SearchFilterService, UserService) {

    init();

    function init() {
        resetParameters();
        getAnnouncements();
        setupWatchMsgLength();
        setupSearchMsg();
        getAdminUser();
    }

    function resetParameters() {
        // Clear the message list
        $scope.msgs = [];
        $scope.finalSearchMsg = '';
        $scope.searchMsg = '';
        //$scope.msgReverse = '+';
        $scope.totalMsgNumber = 0;
        $scope.msgNumberUpperBound = 10;
        $scope.userStartSearch = false;
        $scope.msgLeft = 0;

        $scope.isSearchShown = false;
        /////// for hosptialWarning Message ////
        $rootScope.statusCrumbsNotFound = false;
        ////////////////////////////////////////
    }

    function getAdminUser() {
        UserService.getUser($rootScope.username)
            .then((response) => {
                $scope.adminuser = response.data;
            })
            .catch((error) => {
                // TODO: display error
                console.log(error);
            });
    }

    function getAnnouncements() {
        // Retreive all past annoucements
        AnnouncementService.getAnnouncements()
            .then((response) => {
                // TODO: load on demand (e.g. click/scroll to load more)
                $scope.msgs = response.data;
            })
            .catch((error) => {
                // TODO: display error
                console.log(error);
            });
    }

    $scope.sendPostContent = function() {
        if($scope.postContent && $scope.postContent.length > 0) {
            let annoucement = {
                messageType: 'ANNOUNCEMENT',
                author: $rootScope.username,
                content: $scope.postContent
            };

            // Post a new announcement
            AnnouncementService.postAnnouncement(annoucement)
                .then((response) => {
                    // TODO: mark as "success"
                    $scope.msgs.unshift(response.data); // unshift = push front
                })
                .catch((error) => {
                    // TODO: display error
                    console.log(error);
                });

            $scope.postContent = "";
        }
    };

    function setupWatchMsgLength() {
        $scope.$watch('msgs.length', function() {
            $scope.totalMsgNumber = $scope.msgs.length;
            if($scope.finalSearchMsg === '') {
                $scope.msgNumberUpperBound = $scope.totalMsgNumber;
            }
        });
    }

    function setupSearchMsg() {
        $scope.$watch('searchMsg', function() {
            $scope.finalSearchMsg = SearchFilterService.filterSearchKeywords($scope.searchMsg);
            $scope.msgNumberUpperBound = $scope.finalSearchMsg === '' ? $scope.totalMsgNumber : 10;
            notifyMsgLeftChanged();
        });
    }

    function notifyMsgLeftChanged() {
        $scope.msgLeft = Math.min(10, $scope.filtered.length - $scope.msgNumberUpperBound);
    }

    $scope.msgFilter = function(msg) {
        if ($scope.finalSearchMsg === '') {
            if($scope.searchMsg !== '') {
                // searching only stop words
                return false;
            } else {
                //not in searching, show all
                return true;
            }
        }

        return SearchFilterService.messageHasKeywords(msg, $scope.finalSearchMsg);
    };

    $scope.clearSearch = function() {
        $scope.searchMsg = '';
        $scope.msgNumberUpperBound = $scope.totalMsgNumber;
    };

    $scope.getMore = function() {
        $scope.msgNumberUpperBound += 10;
        notifyMsgLeftChanged();
    };

    $scope.toggleSearchForm = function toggleSearchForm() {
        $scope.clearSearch();
        $scope.isSearchShown = !$scope.isSearchShown;
    };

}

function ScrollTopDirective() {
    return {
        scope: {
            scrollTop: "="
        },
        link(scope, element) {
            scope.$watchCollection('scrollTop', (newValue) => {
                if (newValue) {
                    $(element).scrollTop(0);
                }
            });
        }
    };
}

})();
