(function() {

angular
    .module('FseEsnApp')
    .controller('ResourceSharingController', ResourceSharingController);

ResourceSharingController.$inject = [
    '$scope',
    '$rootScope',
    'ResourceSharingService'
];
function ResourceSharingController($scope, $rootScope, ResourceSharingService) {
    $scope.showSuccess = false;
    $scope.showWarning = false;
    $scope.closeWarning = function() {
        $scope.showSuccess = false;
    };
    $scope.closeSuccess = function() {
        $scope.showSuccess = false;
    };

    $scope.needReloadList = false;
    $scope.isListDisplayed = true;
    $scope.toggleTabs = function() {
        $scope.isListDisplayed = !$scope.isListDisplayed;
        if ($scope.needReloadList && $scope.isListDisplayed) {
            $scope.reloadList();
        }
    };
    $scope.reloadList = function() {
        $scope.needReloadList = false;
        $scope.shareables = [];
        ResourceSharingService.getAllShareableResources().then((response) => {
            $scope.shareables = response.data;
        });
    };
    $scope.reloadList();

    $scope.currentShareableModel = {
        types: {
            'FOOD': false,
            'WATER': false,
            'MEDKIT': false,
            'SHELTER': false
        },
        description: ''
    };
    ResourceSharingService.getShareableResourcesByUser($rootScope.username).then((response) => {
        if (response.data.length > 0) {
            response.data[0].types.split(',').forEach((type) => {
                $scope.currentShareableModel.types[type] = true;
            });
            $scope.currentShareableModel.description = response.data[0].description;
        }
    });

    $scope.submitForm = function() {
        $scope.showSuccess = false;
        $scope.showWarning = false;

        let shareableResource = {};
        shareableResource.types = [];
        for (let type in $scope.currentShareableModel.types) {
            if ($scope.currentShareableModel.types[type]) {
                shareableResource.types.push(type);
            }
        }
        shareableResource.description = $scope.currentShareableModel.description;
        if ($rootScope.geolocationCoords !== null) {
            shareableResource.latitude = $rootScope.geolocationCoords.latitude;
            shareableResource.longitude = $rootScope.geolocationCoords.longitude;
        }

        ResourceSharingService.postShareableResource($rootScope.username, shareableResource).then((response) => {
            $scope.showSuccess = true;
            $scope.needReloadList = true;
        }).catch((err) => {
            if (err.status === 400 &&
                (err.data === 'Invalid shareable resource types' ||
                 err.data === 'The length of description should not exceed 70 characters')) {
                $scope.showWarning = true;
            } else {
                console.log(err);
            }
        });
    };
}

})();
