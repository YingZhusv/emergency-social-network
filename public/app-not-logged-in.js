(function() {

angular
    .module('FseEsnApp', ['ui.router'])
    .config(routeConfig);

routeConfig.$inject = [
    '$stateProvider',
    '$urlRouterProvider'
];
function routeConfig($stateProvider, $urlRouterProvider) {
    // *** Set up UI states ***
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'views/login.html'
        })
        .state('instruction', {
            url: '/instruction',
            templateUrl: 'views/instructionView.html'
        });

    // Redirect to home page if no other URL matches
    $urlRouterProvider.otherwise('/home');
}

})();
