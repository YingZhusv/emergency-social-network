(function() {

angular
    .module('FseEsnApp')
    .factory('ResourceSharingService', ResourceSharingService);

ResourceSharingService.$inject = ['$http'];
function ResourceSharingService($http) {
    return {
        getAllShareableResources() {
            return $http.get('/api/shareables');
        },

        getShareableResourcesByUser(username) {
            return $http.get('/api/users/' + username + '/shareables');
        },

        postShareableResource(username, shareableResource) {
            return $http.post('/api/users/' + username + '/shareables', shareableResource);
        }
    };
}

})();
