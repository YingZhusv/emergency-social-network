(function() {

angular
    .module('FseEsnApp')
    .factory('FootprintsService', FootprintsService);

FootprintsService.$inject = ['$http', '$rootScope', 'SocketService'];
function FootprintsService($http, $rootScope, SocketService) {

    return {
        getFootprints(username) {
            return $http.get('/api/footprints/' + username);
        }
    };
}

})();
