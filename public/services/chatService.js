(function() {

angular
    .module('FseEsnApp')
    .factory('ChatService', ChatService);

ChatService.$inject = ['$http', '$rootScope', 'SocketService'];
function ChatService($http, $rootScope, SocketService) {
    // Set up socket connections
    SocketService.joinRoom('Public'); // public
    SocketService.joinRoom('@' + $rootScope.username); // private

    return {
        postPublicMessage(message) {
            message.socketId = SocketService.getSocketId();
            return $http.post('/api/messages/public', message);
        },

        postPrivateMessage(message) {
            message.socketId = SocketService.getSocketId();
            return $http.post('/api/messages/private', message);
        },

        getPublicMessages() {
            return $http.get('/api/messages/public');
        },

        getPrivateMessages(username1, username2) {
            return $http.get('/api/messages/private/' + username1 + '/' + username2);
        },

        onNewMessage(callback) {
            SocketService.onEvent('newMessage', callback);
        },

        removeNewMessageListener(callback) {
            SocketService.removeEventListener('newMessage', callback);
        }
    };
}

})();
