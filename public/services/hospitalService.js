(function() {

angular
    .module('FseEsnApp')
    .factory('HospitalService', HospitalService);

HospitalService.$inject = [
    '$http',
    '$rootScope',
];

function HospitalService($http, $rootScope) {
    return {

        getHospitalNearby(start_latitude, start_longitude) {
            return $http({
                 url: 'api/maps/hospitals',
                 method: "GET",
                 params: {
                    start_latitude: start_latitude,
                    start_longitude: start_longitude
                 }
            });
        },

        getHospitalEstimateTime(start_latitude, start_longitude) {
            return $http({
                 url: 'api/maps/hospitals/estimates/time',
                 method: "GET",
                 params: {
                    start_latitude: start_latitude,
                    start_longitude: start_longitude
                 }
            });
        },

        getHospitalEstimatePrice(start_latitude, start_longitude, end_latitude, end_longitude) {
            return $http({
                 url: 'api/maps/hospitals/estimates/price',
                 method: "GET",
                 params: {
                    'start_latitude': start_latitude,
                    'start_longitude': start_longitude,
                    'end_longitude': end_longitude,
                    'end_latitude': end_latitude,
                 }
            });
        }
    };
}

})();
