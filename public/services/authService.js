(function() {

angular
    .module('FseEsnApp')
    .factory('AuthService', AuthService);

AuthService.$inject = ['$http'];
function AuthService($http) {
    return {
        signUp: function(data) {
            return $http.post('/api/users/signup', data);
        },

        logIn: function(data) {
            return $http.post('/api/users/login', data);
        },

        logOut: function() {
            return $http.post('/api/users/logout', {});
        }
    };
}

})();
