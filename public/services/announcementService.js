(function() {

angular
    .module('FseEsnApp')
    .factory('AnnouncementService', AnnouncementService);

AnnouncementService.$inject = ['$http'];
function AnnouncementService($http) {
    return {
        postAnnouncement(announcement) {
            return $http.post('/api/messages/announcement', announcement);
        },

        getAnnouncements() {
            return $http.get('/api/messages/announcement');
        }
    };
}

})();
