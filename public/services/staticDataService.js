(function() {

angular
    .module('FseEsnApp')
    .factory('StaticDataService', StaticDataService);

function StaticDataService() {
    return {
        getStatusList: getStatusList
    };

    function getStatusList() {
        var okStatus = {
            status: "OK",
            description: "I am OK, I do not need help.",
            colorCode: "GREEN",
            tableClass: "success"
        };
        var helpStatus = {
            status: "Help",
            description: "I need help, but this is not a life threatening emergency.",
            colorCode: "YELLOW",
            tableClass: "warning"
        };
        var emergencyStatus = {
            status: "Emergency",
            description: "I need help now, as this is a life threatening emergency!",
            colorCode: "RED",
            tableClass: "danger"
        };

        return [okStatus, helpStatus, emergencyStatus];
    }
}

})();
