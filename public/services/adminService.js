(function() {

angular
    .module('FseEsnApp')
    .factory('AdminService', AdminService);

AdminService.$inject = ['$http'];
function AdminService($http) {
    return {
        deactivateUser(username) {
            return $http.post('/api/admin/users/' + username + '/inactive');
        },

        activateUser(username) {
            return $http.post('/api/admin/users/' + username + '/active');
        },

        updateUser(username, newUser) {
            return $http.post('/api/admin/users/' + username, newUser);
        },

        getAllUsers() {
            return $http.get('/api/admin/users');
        }
    };
}

})();
