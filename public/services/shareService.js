(function() {

    angular
        .module('FseEsnApp')
        .factory('ShareService', ShareService);

    ShareService.$inject = ['$http'];
    function ShareService($http) {
        return {
            postShare(share) {
                return $http.post('/api/messages/share', share);
            },

            getShares() {
                return $http.get('/api/messages/share');
            },

            getUserInformation(username) {
                return $http.get('/api/users/' + username);
            }
        };
    }

})();
