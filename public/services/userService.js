(function() {

angular
    .module('FseEsnApp')
    .factory('UserService', UserService);

UserService.$inject = ['$http', 'SocketService'];
function UserService($http, SocketService) {
    // Set up socket connection
    SocketService.joinRoom('UserList');

    return {
        getAllUsers() {
            return $http.get('/api/users');
        },

        getNearbyUsers(longitude, latitude, distance) {
            return $http.get('/api/users/nearby?longitude='+longitude+'&latitude='+latitude+'&distance='+distance);
        },

        getUser(username) {
            return $http.get('/api/users/' + username);
        },

        postStatus(username, statusCrumb) {
            return $http.post('/api/users/' + username + '/statuses', statusCrumb);
        },

        getStatuses(username) {
            return $http.get('/api/users/' + username + '/statuses');
        },

        updateShareFootprintSetting(username, newSetting) {
            return $http.post('/api/users/' + username + '/footprintSetting', {setting: newSetting});
        },

        distanceCalculation: distanceCalculation,
        addDistanceAttribute: addDistanceAttribute,
        removeSpecificUser: removeSpecificUser,
        modifyNearbyUser: modifyNearbyUser

    };
    function modifyNearbyUser(users, latitude, longitude, username) {
        users = removeSpecificUser(users, username);
        users = addDistanceAttribute(users, latitude, longitude, username);
        return users;
    }
    function addDistanceAttribute(users, latitude, longitude, username) {
        for(var i = 0; i < users.length; ++i) {
            if(users[i].username == username) {
                users[i].distance = "";
            } else {
                users[i].distance = distanceCalculation(latitude, longitude, users[i].latitude, users[i].longitude);
            }
        }
        return users;
    }

    function distanceCalculation(lat1, lon1, lat2, lon2) {
        if(lat1 === null || lon1 === null || lat2 === null || lon2 === null ) {
            return "";
        }
        var p = 0.01745329251994329;    // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat2 - lat1) * p) / 2 +
                c(lat1 * p) * c(lat2 * p) *
                (1 - c((lon2 - lon1) * p)) / 2;

        return "(" + (12742 * Math.asin(Math.sqrt(a))).toFixed(1) + " km)"; // 2 * R; R = 6371 km
    }

    function removeSpecificUser(users, username) {
        for(var i = 0; i < users.length; ++i) {
            if(users[i].username == username) {
                users.splice(i, 1);
                break;
            }
        }
        return users;
    }
}

})();
