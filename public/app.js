(function() {

angular
    .module('FseEsnApp', ['ui.router', 'ngMap'])
    .run(runBlock)
    .factory('SocketService', SocketService)
    .config(routeConfig);

runBlock.$inject = ['$rootScope', '$timeout', 'SocketService', 'UserService'];
function runBlock($rootScope, $timeout, SocketService, UserService) {
    // Get username
    $rootScope.username = document.getElementById('username-text').innerHTML;
    $rootScope.nearByUsers = [];
    $rootScope.distance = 5;
    $rootScope.nearbyUpper = -1;

    $rootScope.locationAutoSharingEnabled = false;
    UserService.getUser($rootScope.username)
    .then((response) => {
        if(response.data) {
            $rootScope.locationAutoSharingEnabled = response.data.shareFootprintsSetting === 2;
        }
    });

    // Get user's geolocation coordinates
    $rootScope.statusCrumbsNotFound = false;
    $rootScope.geolocationCoords = null;
    $rootScope.geolocationWatcherId = null;
    function autoUpdateGeolocationCoords() {
        if (navigator.geolocation) {
            $rootScope.geolocationWatcherId = navigator.geolocation.watchPosition((position) => {
                console.log(position);
                $rootScope.geolocationCoords = position.coords;
                if($rootScope.locationAutoSharingEnabled) {
                    SocketService.emit('#location@'+$rootScope.username, 'locationChanged', [position.coords.longitude, position.coords.latitude]);
                }
            }, (positionError) => {
                console.log(positionError);
                navigator.geolocation.clearWatch($rootScope.geolocationWatcherId);
                $timeout(() => autoUpdateGeolocationCoords(), 30000); // Restart after 30 seconds
            }, {
                enableHighAccuracy: true // Higher accuracy but consumes more power
            });
        }
    }
    // Kick off the cycle of auto-updating geolocation coordinates
    autoUpdateGeolocationCoords();
}

function SocketService() {
    const socket = io.connect();
    socket.on("forceLogout", function(msg){
        console.log("forceLogout");
        alert(msg + " You are now logged out.");
        window.location.href = '/';
    });

    return {
        getSocketId() {
            return socket !== null ? socket.id : null;
        },

        joinRoom(roomName) {
            socket.emit('join', roomName);
        },

        leaveRoom(roomName) {
            socket.emit('leave', roomName);
        },

        onEvent(eventName, callback) {
            socket.on(eventName, callback);
        },

        removeEventListener(eventName, callback) {
            socket.removeListener(eventName, callback);
        },

        emit(roomName, eventName, msg) {
            socket.emit("emitToAll", {
                roomName: roomName,
                eventName: eventName,
                msg: msg
            });
        }
    };
}

routeConfig.$inject = [
    '$stateProvider',
    '$urlRouterProvider'
];
function routeConfig($stateProvider, $urlRouterProvider) {
    // *** Set up UI states ***
    $stateProvider
        // User list
        .state('userList', {
            url: '/userList',
            templateUrl: 'views/userListView.html'
        })
        // Public/private chat
        .state('chat', {
            url: '/chat?id',
            templateUrl: 'views/chatView.html',
            reloadOnSearch : false
        })
        // Announcements
        .state('announcements', {
            url: '/announcements',
            templateUrl: 'views/announcementView.html'
        })
        // One-click Uber call
        .state('hospitalInstruction', {
            url: '/hospitalInstruction',
            templateUrl: 'views/hospitalInstruction.html'
        })

        .state('hospital', {
            url: '/hospital',
            templateUrl: 'views/hospital.html'
        })
        // Resoource sharing
        .state('shareables', {
            url: '/shareables',
            templateUrl: 'views/resourceSharingView.html'
        })
        // Instructions
        .state('instruction', {
            url: '/instruction',
            templateUrl: 'views/instructionView.html'
        })
        // Log out
        .state('logout', {
            url: '/logout',
            templateUrl: 'views/logout.html'
        })
        // Admin
        .state('admin', {
            url: '/admin',
            templateUrl: 'views/adminView.html'
        })

        // Profile
        .state('profile', {
            url: '/profile',
            templateUrl: 'views/profileView.html'
        })
        // SharetoTwitter
        .state('sharetotwitter', {
            url: '/share',
            templateUrl: 'views/sharetotwitter.html'
        })

        // Footprints
        .state('footprints', {
            url: '/footprints/:username',
            templateUrl: 'views/footprintsView.html'
        });

    // Redirect to home page if no other URL matches
    $urlRouterProvider.otherwise('/userList');
}

})();
