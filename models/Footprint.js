const MessageDB = require('../database').MessageDB;
const StatusCrumbDB = require('../database').StatusCrumbDB;

class Footprint {

    constructor(type, time, location) {
        this.type = type;
        this.time = time;
        this.location = location;
    }

    static getFootprints(user) {
        var results = [];

        return MessageDB.findAll({
            where: {
                author: user,
                location: {
                    $ne: null
                }
            },
            order: [['id', 'ASC']]
        }).then(function(messages){
            for(var i = 0; i < messages.length; i += 1) {
                var msg = messages[i];
                results.push(new Footprint(msg.messageType + " Message", msg.createdAt, msg.location.coordinates));
            }

            return StatusCrumbDB.findAll({
                where: {
                    username: user,
                    location: {
                        $ne: null
                    }
                }
            });
        }).then(function(statusCrumbs){
            for(var i = 0; i < statusCrumbs.length; i += 1) {
                var crumb = statusCrumbs[i];
                results.push(new Footprint("Status Crumb", crumb.createdAt, crumb.location.coordinates));
            }

            results.sort(Footprint.compare).reverse();

            return new Promise((resolve, reject) => {
                resolve(results);
            });
        });
    }

    static compare(a, b) {
        return a.time.getTime() - b.time.getTime();
    }
}

module.exports = Footprint;
