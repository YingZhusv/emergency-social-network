const MessageDB = require('../database').MessageDB;

class Message {

    static getPublicMessages() {
        return MessageDB.findAll({
            where: {messageType: "PUBLIC"},
            order: [['id', 'ASC']]
        });
    }

    static getPublicMessagesByUsername(username) {
        return MessageDB.findAll({
            where: {messageType: "PUBLIC", author: username},
            order: [['id', 'ASC']]
        });
    }

    static postPublicMessage(username, content, location) {
        return MessageDB.create({
            messageType: 'PUBLIC',
            author: username,
            content: content,
            postedAt: new Date(),
            location: location
        });
    }

    static getPrivateMessage(username1, username2) {
        return MessageDB.findAll({
            where: {
                messageType: "PRIVATE",
                $or: [
                    {
                        author: username1,
                        receiver: username2
                    },
                    {
                        author: username2,
                        receiver: username1
                    }
                ]
            },
            order: [['id', 'ASC']]
        });
    }

    static postPrivateMessage(username, receiver, content, location) {
        return MessageDB.create({
            messageType: 'PRIVATE',
            author: username,
            receiver: receiver,
            content: content,
            postedAt: new Date(),
            location: location
        });
    }

    static getAnnouncements() {
        return MessageDB.findAll({
            where: {messageType: 'ANNOUNCEMENT'},
            order: [['id', 'DESC']]
        });
    }

    static postAnnouncement(username, content) {
        return MessageDB.create({
            messageType: 'ANNOUNCEMENT',
            author: username,
            content: content,
            postedAt: new Date()
        });
    }

    static getShares() {
        return MessageDB.findAll({
            where: {messageType: 'SHARE'},
            order: [['id', 'DESC']]
        });
    }

    static postShare(username, content) {
        return MessageDB.create({
            messageType: 'SHARE',
            author: username,
            content: content,
            postedAt: new Date()
        });
    }
}

module.exports = Message;
