const UserDB = require('../database').UserDB;
const reservedUsernames = require('../helpers/ReservedUsernameList');
const onlineStates = require('../helpers/UserOnlineStates');


function resolveUpdateUserResult(results) {
    return new Promise((resolve, reject) => {
        if(results[1][0]) {
            resolve(results[1][0]);
        } else {
            resolve(null);
        }
    });
}

function checkIfThereAreAdministrators() {
    return UserDB.count({
            where: {
                privilegeLevel: 2,
                isActive: true
            }
        }).then(function(c) {
            if(c <= 1) {
                throw new Error('There should be at least 1 active administrator');
            }

            return Promise.resolve();
        });
}

class User {

    static isUsernameReserved(username) {
        return reservedUsernames.includes(username);
    }

    static isUsernameLengthSufficient(username) {
        return username.length > 2;
    }

    static isUsernameAlphanumeric(username) {
        return /^([a-z0-9]|\.|_|\-)+$/ig.test(username);
    }

    static isUsernameValid(username) {
        return this.isUsernameLengthSufficient(username) &&
            !this.isUsernameReserved(username) &&
            this.isUsernameAlphanumeric(username);
    }

    static isPasswordValid(password) {
        return password.length > 3;
    }

    static getUser(username) {
        return UserDB.findOne({
            where: {username: username},
            attributes: { exclude: ['password']}
        });
    }

    static findOneUser(username) {
        return UserDB.findOne({
            where: {username: username},
        });
    }

    static getAllUsers(includeInactiveUsers) {
        if (includeInactiveUsers) {
            return UserDB.findAll({
                attributes: { exclude: ['password'] },
                order: [['username', 'ASC']]
            });
        } else {
            return UserDB.findAll({
                where: { isActive: true },
                attributes: { exclude: ['password'] },
                order: [['username', 'ASC']]
            });
        }
    }

    static createUser(username, password) {
        if (!this.isUsernameValid(username))
            throw new Error('Incorrect format for username');
        if (!this.isPasswordValid(password))
            throw new Error('Incorrect format for password');

        return UserDB.create({username: username, password: password});
    }

    static updateShareFootprintsSetting(username, footprintsSetting) {
        return UserDB.update({
            'shareFootprintsSetting': footprintsSetting
        }, {
            where: {
                'username': username
            }
        });
    }

    static updateIsActive(username, isActive) {
        var promise = Promise.resolve();

        if(!isActive) {
            promise = promise.then(function(){
                return UserDB.findOne({
                    where: {username: username}
                });
            }).then(function(user) {
                if(!user) {
                    throw new Error('User not found');
                }

                var promise2;

                if(user.privilegeLevel === 2) {
                    promise2 = checkIfThereAreAdministrators();
                } else {
                    promise2 = Promise.resolve();
                }

                return promise2;
            });
        }

        return promise.then(function() {
            return UserDB.update({
                    'isActive': isActive
                }, {
                    where: {
                        'username': username
                    },
                    returning: true
                });
        }).then(function(results){
            return resolveUpdateUserResult(results);
        });
    }

    static updateUser(username, userData) {
        if(userData.username && !this.isUsernameValid(userData.username))
            return Promise.reject(Error('Incorrect format for username'));
        if(userData.password && !this.isPasswordValid(userData.password))
            return Promise.reject(Error('Incorrect format for password'));

        return UserDB.findOne({
            where: {username: username}
        }).then(function(user) {
            if(!user) {
                throw new Error('User not found');
            }

            var promise;

            if(user.privilegeLevel === 2 && userData.privilegeLevel < 2) {
                promise = checkIfThereAreAdministrators();
            } else {
                promise = Promise.resolve();
            }

            return promise.then(function() {
                return UserDB.update(userData, {
                        where: {
                            'username': username
                        },
                        returning: true
                    });
            });
        }).then(function(results){
            if(results[1][0] && userData.password) {
                return UserDB.hashPassword(userData.password)
                .then(function(hash) {
                    return results[1][0].update({
                        password: hash
                    });
                });
            }
            return resolveUpdateUserResult(results);
        });
    }

    static updateTwitter(username, token, tokensecret) {
        return UserDB.update({
            twitterToken: token,
            twitterTokenSecret: tokensecret
        }, {
            where: { username: username },
            returning: true
        });
    }

}

module.exports = User;
