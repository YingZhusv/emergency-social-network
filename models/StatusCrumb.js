const StatusCrumbDB = require('../database').StatusCrumbDB;

class StatusCrumb {

    static getStatuses(username) {
        return StatusCrumbDB.findAll({
            where: {
                username: username
            },
            order: [['id', 'DESC']]
        });
    }

    static createStatusCrumb(username, statusCode, location, locationDesc) {
        return StatusCrumbDB.create({
            username: username,
            statusCode: statusCode,
            location: location,
            locationDesc: locationDesc
        });
    }
}

module.exports = StatusCrumb;
