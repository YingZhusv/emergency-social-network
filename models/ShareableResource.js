const ShareableResourceDB = require('../database').ShareableResourceDB;

class ShareableResource {

    static areInputTypesValid(inputTypes) {
        let isValid = (inputTypes.length !== 0);
        inputTypes.forEach((type) => {
            if (type !== 'FOOD' && type !== 'WATER' && type != 'MEDKIT' && type != 'SHELTER') {
                isValid = false;
            }
        });
        return isValid;
    }

    static post(formData) {
        // Force input types to be an Array
        if (!(formData.types instanceof Array)) {
            formData.types = [formData.types];
        }

        if (!ShareableResource.areInputTypesValid(formData.types)) {
            return Promise.reject(new Error('Invalid shareable resource types'));
        }

        if ('description' in formData && formData.description.length > 70) {
            return Promise.reject(new Error('The length of description should not exceed 70 characters'));
        }

        let location = null;
        if ('latitude' in formData && 'longitude' in formData) {
            location = {
                type: 'POINT',
                coordinates: [formData.longitude, formData.latitude]
            };
        }
        return ShareableResourceDB.create({
            username: formData.username,
            types: formData.types.join(),
            description: formData.description,
            location: location
        });
    }

    static getAll() {
        return ShareableResourceDB.findAll({
            order: [['id', 'DESC']]
        });
    }

    static getByUser(username) {
        return ShareableResourceDB.findAll({
            where: {username: username},
            order: [['id', 'DESC']]
        });
    }
}

module.exports = ShareableResource;
