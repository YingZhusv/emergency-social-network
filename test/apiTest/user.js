const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const expect = chai.expect;

const server = require('../../server');
const Models = require('../../database');

suite('User Authentication', function() {
    //declare test data
    const username1 = 'abcdef';
    const username2 = 'ghijk';
    const usernameTooShort = 'te';
    const usernameReserved = 'test';
    const password1 = 'abcdef';
    const password2 = 'abcdefg';
    const passwordTooShort = 'a';
    const passwordInCorrected = 'asdfsdfs';

    // drop table if existed
    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            done();
        });
    });

    suiteTeardown(function(done) {
        server.close();
        done();
    });

    // test case for getting empty user list
    test('get empty user list', function(done) {
        chai.request(server)
            .get('/api/users')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // test case for user signup
    test('signup user', function(done) {
        chai.request(server)
            .post('/api/users/signup')
            .send({
                'username': username1,
                'password': password1
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.username).to.eql('abcdef');
                done();
            });
    });
    // test case for user list which only have 1 user
    test('check if user list have 1 user', function(done) {
        chai.request(server)
            .get('/api/users')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                expect(res.body[0].username).to.eql(username1);
                done();
            });
    });
    // test case for signing up a existing user
    test('signup existing user', function(done) {
        chai.request(server)
            .post('/api/users/signup')
            .send({
                'username': username1,
                'password': password1
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.username).to.eql(username1);
                done();
            });
    });
    // test case for too short username
    test('signup with too short username', function(done) {
        chai.request(server)
            .post('/api/users/signup')
            .send({
                'username': usernameTooShort,
                'password': password1
            })
            .end((err, res) => {
                expect(res.status).to.eql(400);
                done();
            });
    });
    // test case for too weak password
    test('signup with incorrect format', function(done) {
        chai.request(server)
            .post('/api/users/signup')
            .send({
                'username': username2,
                'password': passwordTooShort
            })
            .end((err, res) => {
                expect(res.status).to.eql(400);
                done();
            });
    });
    // test case for signing up with existing user with incorrect passeword
    test('signup with existing user with incorrect password', function(done) {
        chai.request(server)
            .post('/api/users/signup')
            .send({
                'username': username1,
                'password': passwordInCorrected
            })
            .end((err, res) => {
                expect(res.status).to.eql(401);
                done();
            });
    });
    // test case for logging in existing user with wrong password
    test('login existing user with wrong password', function(done) {
        chai.request(server)
            .post('/api/users/login')
            .send({
                'username': username1,
                'password': passwordInCorrected
            })
            .end((err, res) => {
                expect(res.status).to.eql(401);
                done();
            });
    });
    // test case for logging in with reserved word
    test('login user with incorrect format(username=test)', function(done) {
        chai.request(server)
            .post('/api/users/login')
            .send({
                'username': usernameReserved,
                'password': password1
            })
            .end((err, res) => {
                expect(res.status).to.eql(400);
                done();
            });
    });
    // test case for not exsiting user login
    test('login user that does not exist', function(done) {
        chai.request(server)
            .post('/api/users/login')
            .send({
                'username': username2,
                'password': password2
            })
            .end((err, res) => {
                expect(res.status).to.eql(404);
                done();
            });
    });

    var agent = chai.request.agent(server);
    // test case for existing user login
    test('login existing user', function(done) {
        agent.post('/api/users/login')
            .send({
                'username': username1,
                'password': password1
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                done();
            });
    });
    // test case for previous test case user logout
    test('logout', function(done) {
        agent.post('/api/users/logout')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                done();
            });
    });
    // test case for log out the user who have been already logged out
    test('no need to logout', function(done) {
        agent.post('/api/users/logout')
            .end((err, res) => {
                expect(res.status).to.eql(400);
                done();
            });
    });
    // test case for retrieving user information
    test('retrieve user ' + username1, function(done) {
        agent.get('/api/users/'+username1)
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.username).to.eql(username1);
                done();
            });
    });
    // test case for retrieving not existing user
    test('retrieve user that does not exist (username=test)', function(done) {
        agent.get('/api/users/test')
            .end((err, res) => {
                expect(res.status).to.eql(404);
                done();
            });
    });
});
