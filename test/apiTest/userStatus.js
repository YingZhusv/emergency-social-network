const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const expect = chai.expect;

const server = require('../../server');
const Models = require('../../database');
const StatusCrumb = require('../../models/StatusCrumb');

suite('User Status', function() {
    const agent = chai.request.agent(server);
    const agent1 = chai.request.agent(server);
    const agent2 = chai.request.agent(server);
    const agent3 = chai.request.agent(server);

    const username = 'testuser1';
    const password = '123123123';
    const statusCode = 'GREEN';
    const locationDesc = 'test';
    const latitude = 0;
    const longitude = 0;

    const username_near1 = 'userNear1';
    const statusCode_near1 = 'GREEN';
    const locationDesc_near1 = 'test';
    const latitude_near1 = 0.005;
    const longitude_near1 = 0;

    const username_near2 = 'userNear2';
    const statusCode_near2 = 'GREEN';
    const locationDesc_near2 = 'test';
    const latitude_near2 = 0.04;
    const longitude_near2 = 0;

    const username_near3 = 'userNear3';
    const statusCode_near3 = 'GREEN';
    const locationDesc_near3 = 'test';
    const latitude_near3 = 0.08;
    const longitude_near3 = 0;

    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            agent.post('/api/users/signup')
            .send({
                username: username,
                password: password
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                //done();

                agent1.post('/api/users/signup')
                .send({
                    username: username_near1,
                    password: password
                })
                .end((err, res) => {
                    expect(res.status).to.eql(201);
                    //done();
                    agent2.post('/api/users/signup')
                    .send({
                        username: username_near2,
                        password: password
                    })
                    .end((err, res) => {
                        expect(res.status).to.eql(201);
                        //done();
                        agent3.post('/api/users/signup')
                        .send({
                            username: username_near3,
                            password: password
                        })
                        .end((err, res) => {
                            expect(res.status).to.eql(201);
                            done();
                        });
                    });
                });
            });
        });
    });



    suiteTeardown(function(done) {
        server.close();
        done();
    });

    test('get empty status list for user', function(done) {
        agent.get('/api/users/' + username + '/statuses')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });

    test('post status', function(done) {
        agent.post('/api/users/' + username + '/statuses')
            .send({
                statusCode: statusCode,
                locationDesc: locationDesc,
                latitude: latitude,
                longitude: longitude
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.statusCode).to.eql(statusCode);
                expect(res.body.locationDesc).to.eql(locationDesc);
                expect(res.body.location.coordinates[0]).to.eql(longitude);
                expect(res.body.location.coordinates[1]).to.eql(latitude);
                done();
            });
    });

    test('logout', function(done) {
        agent.post('/api/users/logout')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                done();
            });
    });

    test('post status near1', function(done) {
        agent1.post('/api/users/' + username_near1 + '/statuses')
            .send({
                statusCode: statusCode,
                locationDesc: locationDesc,
                latitude: latitude_near1,
                longitude: longitude_near1
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.statusCode).to.eql(statusCode);
                expect(res.body.locationDesc).to.eql(locationDesc);
                expect(res.body.location.coordinates[0]).to.eql(longitude_near1);
                expect(res.body.location.coordinates[1]).to.eql(latitude_near1);
                done();
            });
    });

    test('post status near2', function(done) {
        agent2.post('/api/users/' + username_near2 + '/statuses')
            .send({
                statusCode: statusCode,
                locationDesc: locationDesc,
                latitude: latitude_near2,
                longitude: longitude_near2
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.statusCode).to.eql(statusCode);
                expect(res.body.locationDesc).to.eql(locationDesc);
                expect(res.body.location.coordinates[0]).to.eql(longitude_near2);
                expect(res.body.location.coordinates[1]).to.eql(latitude_near2);
                done();
            });
    });

    test('post status near3', function(done) {
        agent3.post('/api/users/' + username_near3 + '/statuses')
            .send({
                statusCode: statusCode,
                locationDesc: locationDesc,
                latitude: latitude_near3,
                longitude: longitude_near3
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.statusCode).to.eql(statusCode);
                expect(res.body.locationDesc).to.eql(locationDesc);
                expect(res.body.location.coordinates[0]).to.eql(longitude_near3);
                expect(res.body.location.coordinates[1]).to.eql(latitude_near3);
                done();
            });
    });

    test('check user ' + username + ' \'s coords', function(done) {
        agent.get('/api/users/'+username)
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.latitude).to.eql(latitude);
                expect(res.body.longitude).to.eql(longitude);
                done();
            });
    });

    test('check nearbylist (1km) have 1 user', function(done) {
        chai.request(server)
            .get('/api/users/nearby?longitude='+longitude+'&latitude='+latitude+'&distance=1')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(2);
                done();
            });
    });

    test('check nearbylist (5km) have 2 user', function(done) {
        chai.request(server)
            .get('/api/users/nearby?longitude='+longitude+'&latitude='+latitude+'&distance=5')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(3);
                done();
            });
    });

    test('check nearbylist (10km) have 3 user', function(done) {
        chai.request(server)
            .get('/api/users/nearby?longitude='+longitude+'&latitude='+latitude+'&distance=10')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(4);
                done();
            });
    });

    test('user list have 1 status', function(done) {
        agent.get('/api/users/' + username + '/statuses')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                expect(res.body[0].statusCode).to.eql(statusCode);
                expect(res.body[0].locationDesc).to.eql(locationDesc);
                expect(res.body[0].location.coordinates[0]).to.eql(longitude);
                expect(res.body[0].location.coordinates[1]).to.eql(latitude);
                done();
            });
    });

    test('post status with invalid username', function(done) {
        agent.post('/api/users/invalidUsername/statuses')
            .send({
                statusCode: 'GREEN',
                locationDesc: 'test',
                latitude: latitude,
                longitude: longitude,
            })
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });

});
