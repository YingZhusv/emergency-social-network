const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const User = require('../../models/User');
const Models = require('../../database');

chai.use(chaiHttp);

suite('Message Testing', function() {

    const agent = chai.request.agent(server);
    const coordinatorAgent = chai.request.agent(server);

    const username = '123123';
    const usernameNotLogin = '321321';
    const usernameNotLogin1 = '111111';
    const usernameNotLogin2 = '222222';
    const usernameCoordinator = 'coordinator123';
    const password = '123123';
    const latitude = 0;
    const longitude = 0;
    const content = '' + new Date().getTime();

    suiteSetup(function(done) {
        let access_token = '852653554355453952-Yz39TlKZs360WLuEonJPNPqewhSDq6S';
        let access_tokenSecret = 'bM09dYaokgVkzJANe6XCXoEwNHIoj4shFQDBSmwyvfc8Z';

        Models.sequelize.sync({force: true}).then(function() {
            // Signup to get a session token.
            User.createUser(usernameCoordinator, password)
            .then(function(user) {
                return User.updateUser(usernameCoordinator, {
                    privilegeLevel: 1
                });
            }).then(function(user) {
                agent.post('/api/users/signup')
                    .send({
                        username: username,
                        password: password
                    })
                    .end((err, res) => {
                        User.updateTwitter(username,access_token,access_tokenSecret);
                        coordinatorAgent.post('/api/users/login')
                            .send({
                                username: usernameCoordinator,
                                password: password
                            })
                            .end((err, res) => {
                                done();
                            });
                    });

            });
        });
    });

    suiteTeardown(function(done) {
        done();
    });

    // Test case for posting a public message without any user login
    test('POST /api/messages/public without login', function(done) {
        chai.request(server)
            .post('/api/messages/public')
            .send({
                content: content,
                latitude: latitude,
                longitude: longitude
            })
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });
    // Test case for getting 0 public message
    test('GET /api/messages/public with zero', function(done) {
        chai.request(server)
            .get('/api/messages/public')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // Test case for posting a public message with one user login
    test('POST /api/messages/public with login', function(done) {
        agent.post('/api/messages/public')
            .send({
                content: content,
                latitude: latitude,
                longitude: longitude
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.content).to.eql(content);
                expect(res.body.location.coordinates[0]).to.eql(0);
                expect(res.body.location.coordinates[1]).to.eql(0);
                done();
            });
    });
    // Test case for getting only one public message posted by preivous test case
    test('GET /api/messages/public with one', function(done) {
        agent.get('/api/messages/public')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                done();
            });
    });
    // Test case for getting 0 public message posted by not exsiting user
    test('GET /api/messages/public/{username} with wrong user', function(done) {
        agent.get('/api/messages/public/321321')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // Test case for getting 1 public message posted by login user
    test('GET /api/messages/public/{username} with right user', function(done) {
        agent.get('/api/messages/public/123123')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                done();
            });
    });
    // Test case for posting a private message with no user login
    test('POST /api/messages/private user not login', function(done) {
        chai.request(server)
            .post('/api/messages/private')
            .send({
                username: usernameNotLogin,
                receiver: username,
                content: content,
                latitude: latitude,
                longitude: longitude
            })
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });
    // Test case for posting a private message with user login
    test('POST /api/messages/private user login', function(done) {
        agent.post('/api/messages/private')
            .send({
                receiver: username,
                content: content,
                latitude: latitude,
                longitude: longitude
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.content).to.eql(content);
                expect(res.body.author).to.eql(username);
                expect(res.body.receiver).to.eql(username);
                expect(res.body.location.coordinates[0]).to.eql(0);
                expect(res.body.location.coordinates[1]).to.eql(0);
                done();
            });
    });
    // Test case for getting private messages between two users which are not login
    test('GET /api/messages/private/{username1}/{username2} not login', function(done) {
        chai.request(server)
            .get('/api/messages/private/'+username+'/'+usernameNotLogin)
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });
    // Test case for getting private messages between two users with third user login
    test('GET /api/messages/private/{username1}/{username2} login user access others private message ', function(done) {
        chai.request(server)
            .get('/api/messages/private/'+usernameNotLogin1+'/'+usernameNotLogin2)
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });
    // Test case for getting 0 private message between two users with one of them login
    test('GET /api/messages/private/{username1}/{username2} login but with no private message between two users', function(done) {
        agent.get('/api/messages/private/'+username+'/'+usernameNotLogin)
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // Test case for getting one private message between two users with one of them login
    test('GET /api/messages/private/{username1}/{username2} login with private message between two users', function(done) {
        agent.get('/api/messages/private/'+username+'/'+username)
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                done();
            });
    });
    // Test case for getting 0 announcement
    test('GET /api/messages/announcement with zero announcement', function(done) {
        agent.get('/api/messages/announcement')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // Test case for posting a announcement without coordinator login
    test('POST /api/messages/announcement with normal user (should fail)', function(done) {
        agent.post('/api/messages/announcement')
            .send({
                content: content,
            })
            .end((err, res) => {
                expect(res.status).to.eql(403);
                done();
            });
    });
    // Test case for getting no announcement
    test('GET /api/messages/announcement with 0 announcement', function(done) {
        agent.get('/api/messages/announcement')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });
    // Test case for posting a announcement with coordinator login
    test('POST /api/messages/announcement coordinator logged in', function(done) {
        coordinatorAgent.post('/api/messages/announcement')
            .send({
                content: content,
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.content).to.eql(content);
                expect(res.body.messageType).to.eql('ANNOUNCEMENT');
                done();
            });
    });
    // Test case for getting only one announcement posted by previous test case's user
    test('GET /api/messages/announcement with one announcement', function(done) {
        agent.get('/api/messages/announcement')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                done();
            });
    });
    // Test case for posting a share with coordinator login
    test('POST /api/messages/share coordinator login', function(done) {
        agent.post('/api/messages/share')
            .send({
                content: content,
            })
            .end((err, res) => {
                expect(res.status).to.eql(201);
                expect(res.body.content).to.eql(content);
                expect(res.body.messageType).to.eql('SHARE');
                done();
            });
    });
    // Test case for getting only one share posted by previous test case's user
    test('GET /api/messages/share with one share', function(done) {
        agent.get('/api/messages/share')
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(1);
                done();
            });
    });
    // close the server
    suiteTeardown(function(done) {
        server.close();
        done();
    });
});
