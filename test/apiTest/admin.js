const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const expect = chai.expect;

const server = require('../../server');
const Models = require('../../database');
const User = require('../../models/User');

suite('User Authentication', function() {
    //declare test data
    const adminUser = 'ESNAdmin';
    const adminPwd = 'admin';
    const username1 = ".__.";
    const password1 = "test1";
    const username2 = "T__T";
    const password2 = "test2";
    const changedUsername = '.___.';
    const changedPassword = 'test12';
    const changeprivilegeLevel = 1;
    const changeIsActive = false;

    var adminAgent = chai.request.agent(server);

    // drop table if existed
    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            return User.createUser(username1, password2);
        }).then(function(createdUser) {
            return User.createUser(username2, password2);
        }).then(function(createdUser) {
            return Models.UserDB.create({
                username: "ESNAdmin",
                password: "admin",
                privilegeLevel: 2,
                statusCode: "GREEN"
            });
        }).then(function(createdUser){
            adminAgent.post('/api/users/login')
                .send({
                    'username': adminUser,
                    'password': adminPwd
                })
                .end((err, res) => {
                    done();
                });
        });
    });

    suiteTeardown(function(done) {
        server.close();
        done();
    });

    test('set user active', function(done) {
        adminAgent.post('/api/admin/users/' + username1 + '/active')
                .send()
                .end((err, res) => {
                    expect(res.status).to.eql(200);
                    expect(res.body.username).to.eql(username1);
                    expect(res.body.isActive).to.eql(true);
                    done();
                });
    });

    test('set user inactive', function(done) {
        adminAgent.post('/api/admin/users/' + username2 + '/inactive')
            .send()
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.username).to.eql(username2);
                expect(res.body.isActive).to.eql(false);
                done();
            });
    });

    test('get all users', function(done) {
        adminAgent.get('/api/admin/users')
                .end((err, res) => {
                    expect(res.status).to.eql(200);
                    expect(res.body.length).to.eql(3);
                    done();
                });
    });

    test('get all users', function(done) {
        adminAgent.get('/api/admin/users')
                .end((err, res) => {
                    expect(res.status).to.eql(200);
                    expect(res.body.length).to.eql(3);
                    done();
                });
    });

    test('updateUser user username', function(done) {
        adminAgent.post('/api/admin/users/' + username1)
            .send({
                'username': changedUsername,
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.username).to.eql(changedUsername);
                done();
            });
    });

    test('updateUser user privilegeLevel', function(done) {
        adminAgent.post('/api/admin/users/' + changedUsername)
            .send({
                'privilegeLevel': changeprivilegeLevel,
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.privilegeLevel).to.eql(changeprivilegeLevel);
                done();
            });
    });

});
