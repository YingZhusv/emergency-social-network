const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');

chai.use(chaiHttp);

suite('Hospital API Testing', function() {

    // Environment setup
    suiteSetup(function(done) {
        done();
    });
    // close the server

    test('GET /api/maps/hospitals return 3 hospitals', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals')
            .query({
                start_latitude: '37.4104296',
                start_longitude: '-122.059753'
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.least(3);
                done();
            });
    });

    test('GET /api/maps/hospitals return 0 hospitals', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals')
            .query({
                start_latitude: '37.5133439',
                start_longitude: '-123.559004'
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });

    test('GET /api/maps/hospitals/estimates/time return normal uber estimed time', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals/estimates/time')
            .query({
                start_latitude: '37.4104296',
                start_longitude: '-122.059753'
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.least(1);
                done();
            });
    });

    test('GET /api/maps/hospitals/estimates/time with no uber car', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals/estimates/time')
            .query({
                start_latitude: '37.5133439',
                start_longitude: '-123.559004'
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.eql(0);
                done();
            });
    });

    test('GET /maps/hospitals/estimates/price return normal uber estimed price', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals/estimates/price')
            .query({
                start_latitude: '37.4104296',
                start_longitude: '-122.059753',
                end_latitude: '37.3694094',
                end_longitude: '-122.0802214',
            })
            .end((err, res) => {
                expect(res.status).to.eql(200);
                expect(res.body.length).to.least(1);
                done();
            });
    });

    test('GET /maps/hospitals/estimates/price return 0 result', function(done) {
        chai.request(server)
            .get('/api/maps/hospitals/estimates/price')
            .query({
                start_latitude: '37.4104296',
                start_longitude: '-122.059753',
                end_latitude: '25.049348',
                end_longitude: '121.503835'
            })
            .end((err, res) => {
                expect(res.status).to.eql(422);
                done();
            });
    });

    suiteTeardown(function(done) {
        server.close();
        done();
    });

});
