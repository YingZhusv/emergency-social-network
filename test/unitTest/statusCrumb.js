const expect = require('chai').expect;
const chai = require('chai');
const StatusCrumb = require('../../models/StatusCrumb');
const User = require('../../models/User');
const Models = require('../../database/index');

suite('StatusCrumb Unit Testing', function() {

    const username = 'Edward';
    const password = 'abcde';
    const statusCode = 'GREEN';
    const locationDesc = 'test';
    const location = {
        type: 'POINT',
        coordinates: [1.0, 2.0]
    };
    let user;

    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            User.createUser(username, password)
            .then(function(createdUser) {
                if (createdUser) {
                    user = createdUser;
                }
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    suiteTeardown(function(done) {
        done();
    });

    test('getStatuses('+username+')', function(done) {
        StatusCrumb.getStatuses(username).then(function(statusCrumb) {
            expect(statusCrumb.length).to.be.eql(0);
            done();
        });
    });

    test('createStatusCrumb()', function(done) {
        StatusCrumb.createStatusCrumb(username, statusCode, location, locationDesc)
        .then(function(statusCrumb) {
            expect(statusCrumb.username).to.be.eql(username);
            expect(statusCrumb.statusCode).to.be.eql(statusCode);
            expect(statusCrumb.location.coordinates[0]).to.be.eql(1.0);
            expect(statusCrumb.location.coordinates[1]).to.be.eql(2.0);
            expect(statusCrumb.locationDesc).to.be.eql(locationDesc);
            done();
        });
    });

    test('getStatuses('+username+')', function(done) {
        StatusCrumb.getStatuses(username).then(function(statusCrumb) {
            expect(statusCrumb.length).to.be.eql(1);
            expect(statusCrumb[0].username).to.be.eql(username);
            expect(statusCrumb[0].statusCode).to.be.eql(statusCode);
            expect(statusCrumb[0].locationDesc).to.be.eql(locationDesc);
            expect(statusCrumb[0].location.coordinates[0]).to.be.eql(1.0);
            expect(statusCrumb[0].location.coordinates[1]).to.be.eql(2.0);
            done();
        }).catch(function (err) {
            done(err);
        });
    });

});
