const expect = require('chai').expect;
const DB = require('../../database');
const User = require('../../models/User');
const ShareableResource = require('../../models/ShareableResource');

suite('ShareableResource Unit Testing', () => {
    let user;
    const username = 'edward';
    const invalidUsername = 'edward123';
    const password = '123123';
    const allTypes = ['FOOD', 'WATER', 'MEDKIT', 'SHELTER'];
    const oneType = ['FOOD'];
    const oneTypeStr = 'MEDKIT';
    const emptyTypes = [];
    const invalidTypes = ['TV', 'WATER'];
    const invalidTypes2 = ['FOODWATER', 'MEDKITSHELTER'];
    const description = 'test';
    const longDescription = '12345678901234567890123456789012345678901234567890123456789012345678901234567890';
    const emptyDescription = '';
    const latitude = 45.0;
    const longitude = 123.0;

    suiteSetup((done) => {
        // Reset DB and insert a test user
        DB.sequelize.sync({force: true}).then(() => {
            User.createUser(username, password).then((newUser) => {
                user = newUser;
                done();
            }).catch((err) => {
                done(err);
            });
        });
    });

    suiteTeardown((done) => {
        done();
    });

    test('ShareableResource.post()', (done) => {
        ShareableResource.post({
            username: username,
            types: allTypes,
            description: description,
            latitude: latitude,
            longitude: longitude
        }).then((shareableResource) => {
            expect(shareableResource.username).to.be.eql(username);
            expect(shareableResource.types).to.be.eql(allTypes.join());
            expect(shareableResource.description).to.be.eql(description);
            expect(shareableResource.location.coordinates[0]).to.be.eql(longitude);
            expect(shareableResource.location.coordinates[1]).to.be.eql(latitude);
            done();
        });
    });

    test('ShareableResource.post() - invalid user', (done) => {
        ShareableResource.post({
            username: invalidUsername,
            types: allTypes,
            description: description,
            latitude: latitude,
            longitude: longitude
        }).catch((err) => {
            done();
        });
    });

    test('ShareableResource.post() - minimum post', (done) => {
        ShareableResource.post({
            username: username,
            types: oneType
        }).then((shareableResource) => {
            expect(shareableResource.username).to.be.eql(username);
            expect(shareableResource.types).to.be.eql(oneType.join());
            expect(shareableResource.description).to.not.exist;
            expect(shareableResource.location).to.not.exist;
            done();
        });
    });

    test('ShareableResource.post() - one input type in string', (done) => {
        ShareableResource.post({
            username: username,
            types: oneTypeStr
        }).then((shareableResource) => {
            expect(shareableResource.username).to.be.eql(username);
            expect(shareableResource.types).to.be.eql(oneTypeStr);
            expect(shareableResource.description).to.not.exist;
            expect(shareableResource.location).to.not.exist;
            done();
        });
    });

    test('ShareableResource.post() - empty types', (done) => {
        ShareableResource.post({
            username: username,
            types: emptyTypes
        }).catch((err) => {
            done();
        });
    });

    test('ShareableResource.post() - invalid types: ' + invalidTypes, (done) => {
        ShareableResource.post({
            username: username,
            types: invalidTypes
        }).catch((err) => {
            done();
        });
    });

    test('ShareableResource.post() - invalid types: ' + invalidTypes2, (done) => {
        ShareableResource.post({
            username: username,
            types: invalidTypes2
        }).catch((err) => {
            done();
        });
    });

    test('ShareableResource.post() - long description: ' + longDescription, (done) => {
        ShareableResource.post({
            username: username,
            types: emptyTypes,
            description: longDescription
        }).catch((err) => {
            done();
        });
    });

    test('ShareableResource.getAll()', (done) => {
        ShareableResource.getAll().then((shareables) => {
            expect(shareables.length).to.be.eql(3);
            done();
        });
    });

    test('ShareableResource.getByUser()', (done) => {
        ShareableResource.getByUser(username).then((shareables) => {
            expect(shareables.length).to.be.eql(3);
            done();
        });
    });

    test('ShareableResource.getByUser() - no such user', (done) => {
        ShareableResource.getByUser(invalidUsername).then((shareables) => {
            expect(shareables).to.be.empty;
            done();
        });
    });
});
