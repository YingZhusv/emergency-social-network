const expect = require('chai').expect;
const chai = require('chai');
const server = require('../../server');
const User = require('../../models/User');
const Message = require('../../models/Message');
const Models = require('../../database/index');

suite('Message Unit Testing', function() {

    const username = 'Edward';
    const username2 = 'Edward2';
    const password = 'test';
    const content = 'test';
    const location = {
        type: 'POINT',
        coordinates: [1.0, 2.0]
    };
    let user;
    let user2;

    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            // for postPublicMessage()/postPrivateMessage() because author need to be in user table
            User.createUser(username, password).then(function(createdUser) {
                user = createdUser;
                User.createUser(username2, password).then(function(createdUser2) {
                    user2 = createdUser2;
                    done();
                }).catch(function(err) {
                    console.log(err);
                });
            }).catch(function(err) {
                console.log(err);
            });
        });
    });

    suiteTeardown(function(done) {
        done();
    });

    test('getPublicMessages()', function(done) {
        Message.getPublicMessages().then(function(message) {
            expect(message.length).to.be.eql(0);
            done();
        });
    });

    test('postPublicMessage()', function(done) {
        Message.postPublicMessage(username, content, location)
        .then(function(message) {
            expect(message.author).to.be.eql(username);
            expect(message.content).to.be.eql(content);
            expect(message.messageType).to.be.eql('PUBLIC');
            expect(message.location.coordinates[0]).to.be.eql(1.0);
            expect(message.location.coordinates[1]).to.be.eql(2.0);
            done();
        });
    });

    test('getPublicMessages()', function(done) {
        Message.getPublicMessages().then(function(message) {
            expect(message.length).to.be.eql(1);
            expect(message[0].author).to.be.eql(username);
            expect(message[0].content).to.be.eql(content);
            expect(message[0].messageType).to.be.eql('PUBLIC');
            expect(message[0].location.coordinates[0]).to.be.eql(1.0);
            expect(message[0].location.coordinates[1]).to.be.eql(2.0);
            done();
        });
    });

    test('getPrivateMessage()', function(done) {
        Message.getPrivateMessage(username, username2)
        .then(function(message) {
            expect(message.length).to.be.eql(0);
            done();
        });
    });

    test('postPrivateMessage()', function(done) {
        Message.postPrivateMessage(username, username2, content, location).then(function(message) {
            expect(message.author).to.be.eql(username);
            expect(message.receiver).to.be.eql(username2);
            expect(message.content).to.be.eql(content);
            expect(message.messageType).to.be.eql('PRIVATE');
            expect(message.location.coordinates[0]).to.be.eql(1.0);
            expect(message.location.coordinates[1]).to.be.eql(2.0);
            done();
        });
    });

    test('getPrivateMessage()', function(done) {
        Message.getPrivateMessage(username, username2)
        .then(function(message) {
            expect(message.length).to.be.eql(1);
            expect(message[0].author).to.be.eql(username);
            expect(message[0].receiver).to.be.eql(username2);
            expect(message[0].content).to.be.eql(content);
            expect(message[0].messageType).to.be.eql('PRIVATE');
            expect(message[0].location.coordinates[0]).to.be.eql(1.0);
            expect(message[0].location.coordinates[1]).to.be.eql(2.0);
            done();
        });
    });

    test('getAnnouncements()', function(done) {
        Message.getAnnouncements()
        .then(function(announcement) {
            expect(announcement.length).to.be.eql(0);
            done();
        });
    });

    test('postAnnouncement()', function(done) {
        Message.postAnnouncement(username, content).then(function(announcement) {
            expect(announcement.author).to.be.eql(username);
            expect(announcement.content).to.be.eql(content);
            expect(announcement.messageType).to.be.eql('ANNOUNCEMENT');
            done();
        });
    });

    test('getAnnouncements()', function(done) {
        Message.getAnnouncements()
        .then(function(announcement) {
            expect(announcement.length).to.be.eql(1);
            expect(announcement[0].author).to.be.eql(username);
            expect(announcement[0].content).to.be.eql(content);
            expect(announcement[0].messageType).to.be.eql('ANNOUNCEMENT');
            done();
        });
    });

    test('getShares()', function(done) {
        Message.getShares()
            .then(function(share) {
                expect(share.length).to.be.eql(0);
                done();
            });
    });

    test('postShare()', function(done) {
        Message.postShare(username, content).then(function(share) {
            expect(share.author).to.be.eql(username);
            expect(share.content).to.be.eql(content);
            expect(share.messageType).to.be.eql('SHARE');
            done();
        });
    });

    test('getShares()', function(done) {
        Message.getShares()
            .then(function(share) {
                expect(share.length).to.be.eql(1);
                expect(share[0].author).to.be.eql(username);
                expect(share[0].content).to.be.eql(content);
                expect(share[0].messageType).to.be.eql('SHARE');
                done();
            });
    });
});
