const expect = require('chai').expect;
const onlineStates = require('../../helpers/UserOnlineStates');

suite('User Online State', function() {
    suiteSetup(function(done) {
        done();
    });

    suiteTeardown(function(done) {
        done();
    });

    test('add socket', function(done) {
        expect(onlineStates.getSocket("user", "123")).to.eql(null);
        onlineStates.addSocket("user", {id: "123"});
        expect(onlineStates.getSocket("user", "123")).to.not.eql(null);
        done();
    });

    test('test online', function(done) {
        expect(onlineStates.isOnline("user")).to.eql(true);
        done();
    });

    test('remove socket', function(done) {
        expect(onlineStates.getSocket("user", "123")).to.not.eql(null);
        onlineStates.removeSocket("user", {id: "123"});
        expect(onlineStates.isOnline("user")).to.eql(false);
        done();
    });

});
