const expect = require('chai').expect;
const chai = require('chai');
const User = require('../../models/User');
const Models = require('../../database/index');
const util = require('util');

suite('User Unit Testing', function() {

    const username = 'Edward';
    const username2 = 'Edward2';
    const usernameUpdated = 'Edward123';
    const usernameReserved = 'test';
    const usernameLengthNotSufficient = 'te';
    const usernameNotAlphanumeric = '****';
    const password = 'abcde';
    const password2 = 'fjhi';
    const passwordInValid = 'abc';
    const passwordUpdated = 'updated';
    const lastLoginAt = new Date();
    const statusCode = 'UNDEFINED';
    const token = 'test1';
    const tokensecret = 'test2';
    const longitude = 78;
    const latitude = 87;
    const location = {
        type: 'POINT',
        coordinates: [longitude, latitude]
    };

    var user;
    var userInformatUsername;
    var userInformatPassword;

    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            User.createUser(username, password)
            .then(function(createdUser) {
                if (createdUser) {
                    user = createdUser;
                }
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        // this's for 100% test coverage
        // User.createUser(usernameLengthNotSufficient, password)
        // .then(function(createdUser) {
        //     if (createdUser) {
        //         userInformatUsername = createdUser;
        //     }
        //     // done();
        // }).catch(function (err) {
        // });
        // User.createUser(username, passwordInValid)
        // .then(function(createdUser) {
        //     if (createdUser) {
        //         userInformatPassword = createdUser;
        //     }
        //     done();
        // }).catch(function (err) {
        // });
    });

    suiteTeardown(function(done) {
        done();
    });

    test('isUsernameReserved(username='+username+')', function(done) {
        expect(User.isUsernameReserved(username)).to.eql(false);
        done();
    });

    test('isUsernameReserved(username='+usernameReserved+')', function(done) {
        expect(User.isUsernameReserved(usernameReserved)).to.eql(true);
        done();
    });

    test('isUsernameLengthSufficient(username='+username+')', function(done) {
        expect(User.isUsernameLengthSufficient(username)).to.eql(true);
        done();
    });

    test('isUsernameLengthSufficient(username='+usernameLengthNotSufficient+')', function(done) {
        expect(User.isUsernameLengthSufficient(usernameLengthNotSufficient)).to.eql(false);
        done();
    });

    test('isUsernameAlphanumeric(username='+username+')', function(done) {
        expect(User.isUsernameAlphanumeric(username)).to.eql(true);
        done();
    });

    test('isUsernameAlphanumeric(username='+usernameNotAlphanumeric+')', function(done) {
        expect(User.isUsernameAlphanumeric(usernameNotAlphanumeric)).to.eql(false);
        done();
    });

    test('isUsernameValid(username='+username+')', function(done) {
        expect(User.isUsernameValid(username)).to.eql(true);
        done();
    });

    test('isUsernameValid(username)', function(done) {
        expect(User.isUsernameValid(usernameReserved)).to.eql(false);
        expect(User.isUsernameValid(usernameNotAlphanumeric)).to.eql(false);
        expect(User.isUsernameValid(usernameLengthNotSufficient)).to.eql(false);
        done();
    });

    test('isPasswordValid(password='+password+')', function(done) {
        expect(User.isPasswordValid(password)).to.eql(true);
        done();
    });

    test('isPasswordValid(password='+passwordInValid+')', function(done) {
        expect(User.isPasswordValid(passwordInValid)).to.eql(false);
        done();
    });

    test('validatePassword(password, callback)', function(done) {
        user.validatePassword(password, function(valid) {
            expect(valid).to.eql(true);
            done();
        });
    });

    test('validatePassword(password, callback)', function(done) {
        user.validatePassword(password2, function(valid) {
            expect(valid).to.eql(false);
            done();
        });
    });

    test('updateLastLoginAt()', function(done) {
        user.updateLastLoginAt();
        expect(user.lastLoginAt.getTime() > lastLoginAt.getTime()).to.eql(true);
        done();
    });

    test('safeOutput()', function(done) {
        var data = user.safeOutput();
        expect(data.username).to.eql(username);
        expect(data.password).to.eql(undefined);
        done();
    });

    test('createUser(username='+username2+', password = '+password+')', function(done) {
        User.createUser(username2, password).then(function(user) {
            expect(user).to.be.an('Object');
            expect(user.username).to.be.eql(username2);
            done();
        }).catch(function(err) {
            console.log(err);
            done(err);
        });
    });

    test('findOneUser(username='+username+')', function(done) {
        User.findOneUser(username).then(function(user) {
            expect(user).to.be.an('Object');
            expect(user.username).to.be.eql(username);
            done();
        });
    });

    test('getAllUsers()', function(done) {
        User.getAllUsers().then(function(user) {
            expect(user.length).to.be.eql(2);
            done();
        });
    });

    test('updateStatusCode()', function(done) {
        expect(user.statusCode).to.be.eql('UNDEFINED');
        user.updateStatusCode(statusCode);
        expect(user.statusCode).to.be.eql(statusCode);
        done();
    });

    test('updateTwitter(username='+username+', token='+token+', tokensecret='+tokensecret+')', function(done) {
        User.updateTwitter(username, token, tokensecret).then(function(response) {
            let user = response[1][0].dataValues;
            expect(user).to.be.an('Object');
            expect(user.twitterToken).to.be.eql(token);
            expect(user.twitterTokenSecret).to.be.eql(tokensecret);
            done();
        });
    });

    test('updateLocation()', function(done) {
        user.updateLocation(location);
        expect(user.longitude).to.be.eql(longitude);
        expect(user.latitude).to.be.eql(latitude);
        done();
    });

    test('updateIsActive(false)', function(done) {
        User.updateIsActive(username, false)
        .then(function(user){
            expect(user.isActive).to.be.eql(false);
            done();
        });
    });

    test('getAllUsers() - exclude user ' + username, function(done) {
        User.getAllUsers().then(function(user) {
            expect(user.length).to.be.eql(1);
            done();
        });
    });

    test('updateIsActive(true)', function(done) {
        User.updateIsActive(username, true)
        .then(function(user){
            expect(user.isActive).to.be.eql(true);
            done();
        });
    });

    test('update user with reserved username', function(done) {
        User.updateUser(username, {
            username: usernameReserved,
        }).catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });

    test('update user with username too short', function(done) {
        User.updateUser(username, {
            username: usernameLengthNotSufficient,
        }).catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });

    test('update user with non alphanumeric username', function(done) {
        User.updateUser(username, {
            username: usernameNotAlphanumeric,
        }).catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });


    test('update user with invalid password', function(done) {
        User.updateUser(username, {
            password: passwordInValid,
        }).catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });

    test('update user', function(done) {
        User.updateUser(username, {
            username: usernameUpdated,
            password: passwordUpdated,
            privilegeLevel: 2
        }).then(function(user) {
            expect(user.privilegeLevel).to.be.eql(2);
            expect(user.username).to.be.eql(usernameUpdated);

            return User.findOneUser(user.username);
        }).then(function(user) {
            user.validatePassword(passwordUpdated, function(valid) {
                expect(valid).to.eql(true);
                done();
            });
        });
    });

    test('update user trying to have 0 active admin', function(done) {
        User.updateUser(usernameUpdated, {
            username: usernameUpdated,
            password: passwordUpdated,
            privilegeLevel: 0
        }).catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });

    test('updateIsActive(false) trying to have 0 active admin', function(done) {
        User.updateIsActive(usernameUpdated, false)
        .catch(function(err) {
            expect(err).to.not.eql(null);
            done();
        });
    });
});
