const expect = require('chai').expect;
const chai = require('chai');
const User = require('../../models/User');
const Models = require('../../database/index');

const Footprint = require('../../models/Footprint');
const Message = require('../../models/Message');
const StatusCrumb = require('../../models/StatusCrumb');

suite('Footprints Unit Testing', function() {

    const username = 'Edward';
    const password = 'abcde';
    var user;

    suiteSetup(function(done) {
        Models.sequelize.sync({force: true}).then(function() {
            User.createUser(username, password)
            .then(function(createdUser) {
                if (createdUser) {
                    user = createdUser;
                }
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    suiteTeardown(function(done) {
        done();
    });

    test('compare function of Footprints', function(done) {
        let fp1 = new Footprint('', new Date(100), [0,0]);
        let fp2 = new Footprint('', new Date(0), [0,0]);

        expect(Footprint.compare(fp1, fp2)).to.gt(0);
        expect(Footprint.compare(fp2, fp1)).to.lt(0);
        expect(Footprint.compare(fp2, fp2)).to.eq(0);

        done();
    });

    test('get empty result of Footprints', function(done) {
        Footprint.getFootprints(username)
        .then(function(footprints) {
            expect(footprints.length).to.eq(0);
            done();
        });
    });

    test('post message and get result of Footprints', function(done) {
        const testLocation = {
            type: 'POINT',
            coordinates: [60, 60]
        };

        Message.postPublicMessage(username, "MSG", testLocation)
        .then(function(msg){
            return Footprint.getFootprints(username);
        }).then(function(footprints) {
            expect(footprints.length).to.eq(1);
            let footprint = footprints[0];
            expect(footprint.location).to.deep.eq(testLocation.coordinates);
            done();
        });
    });

    test('post status crumb and get result of Footprints', function(done) {
        const testLocation2 = {
            type: 'POINT',
            coordinates: [80, 80]
        };

        StatusCrumb.createStatusCrumb(username, "GREEN", testLocation2, "LOCATION")
        .then(function(msg){
            return Footprint.getFootprints(username);
        }).then(function(footprints) {
            expect(footprints.length).to.eq(2);
            let footprint = footprints[0];
            expect(footprint.location).to.deep.eq(testLocation2.coordinates);
            done();
        });
    });

    test('update footprints setting to auto', function(done) {
        User.updateShareFootprintsSetting(username, 2)
        .then(function(){
            return User.getUser(username);
        }).then(function(user) {
            expect(user.shareFootprintsSetting).to.eq(2);
            done();
        });
    });

    test('update footprints setting to on', function(done) {
        User.updateShareFootprintsSetting(username, 1)
        .then(function(){
            return User.getUser(username);
        }).then(function(user) {
            expect(user.shareFootprintsSetting).to.eq(1);
            done();
        });
    });

    test('update footprints setting to off', function(done) {
        User.updateShareFootprintsSetting(username, 0)
        .then(function(){
            return User.getUser(username);
        }).then(function(user) {
            expect(user.shareFootprintsSetting).to.eq(0);
            done();
        });
    });

});
