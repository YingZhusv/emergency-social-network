'use strict';

const debug = require('debug')('s17-esn-sv1:server');
const http = require('http');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const expressSession = require('express-session');
const passport = require('passport');
const Sequelize = require('sequelize');
const SequelizeStore = require('connect-session-sequelize')(expressSession.Store);
const Models = require('./database/index');

const socketController = require('./controllers/socketController');

const index = require('./routes/index');
const apis = require('./routes/api');

const app = express();

app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());
var sequelizeStore = new SequelizeStore({
        db: Models.sequelize
        // checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions in milliseconds.
        // expiration: 24 * 60 * 60 * 1000  // The maximum age (in milliseconds) of a valid session.
    });
var sessionMiddleware = expressSession({
    secret: process.env.SESSION_SECRET,
    store: sequelizeStore,
    resave: false,
    saveUninitialized: false,
    unset: 'destroy'
});
sequelizeStore.sync();
app.use(sessionMiddleware);

app.use(passport.initialize());
app.use(passport.session());

app.use('/', index);
app.use('/api', apis.router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = process.env.NODE_ENV === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500).send(err.status + " error");
    console.log(err);
});

const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);
socketController.init(server, sessionMiddleware);

server.listen(port, () => {
    debug('Listening on port ' + port);
});

module.exports = server;
