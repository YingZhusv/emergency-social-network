# s17-ESN-SV1
Master: ![Master Build status](https://circleci.com/gh/cmusv-fse/s17-ESN-SV1.svg?&style=shield&circle-token=b56918201716f3c46bc89c53112d4f9b1a358485) Develop: ![Develop Build status](https://circleci.com/gh/cmusv-fse/s17-ESN-SV1/tree/develop.svg?style=shield&circle-token=b56918201716f3c46bc89c53112d4f9b1a358485) Better Code Hub: [![BCH compliance](https://bettercodehub.com/edge/badge/cmusv-fse/s17-ESN-SV1?token=2513769aa7db3c740642cb027db081466db680dc)](https://bettercodehub.com/)

YOU ARE NOT PERMITTED TO SHARE THIS REPO OUTSIDE THIS GITHUB ORG.
YOU ARE NOT PERMITTED TO FORK THIS REPO.
YOU ARE NOT ALLOWED TO MAKE ANY PUBLIC REPOS INSIDE cmusv-fse

* Production: https://s17-esn-sv1.herokuapp.com/
* Alpha: https://s17-esn-sv1-alpha.herokuapp.com/

