const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');

router.get('/', function(req, res, next) {

    if(req.user) {
        if(!req.user.isActive) {
            req.logout();
            res.redirect(307, "/");
            return;
        }

        res.render('../views/index', {'username': req.user.username, 'privilegeLevel': req.user.privilegeLevel});
    } else {
        res.render('../views/index-not-logged-in');
    }
});

module.exports = router;
