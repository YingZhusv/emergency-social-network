const express = require('express');
const passport = require('passport');
const router = express.Router();

const authController = require('../controllers/authController');
const userController = require('../controllers/userController');
const publicChatController = require('../controllers/publicChatController');
const privateChatController = require('../controllers/privateChatController');
const announcementController = require('../controllers/announcementController');
const shareController = require('../controllers/shareController');
const hospitalController = require('../controllers/hospitalController');
const footprintController = require('../controllers/footprintController');
const resourceSharingController = require('../controllers/resourceSharingController');
const adminController = require('../controllers/adminController');

router.post('/users/signup', authController.signup);
router.post('/users/login', authController.login);
router.post('/users/logout', authController.logout);

router.get('/users/twitter/login', authController.share);
router.get('/users/twitter/callback', authController.callback);

router.get('/users', userController.getAllUsers);
router.get('/users/nearby', userController.getNearbyUsers);
router.get('/users/:username', userController.getUser);

router.post('/users/:username/statuses', authController.authenticate, userController.postStatus);
router.get('/users/:username/statuses', userController.getStatuses);

router.post('/messages/public', authController.authenticate, publicChatController.postMessage);
router.get('/messages/public', publicChatController.getMessages);
router.get('/messages/public/:username', publicChatController.getMessagesByUsername);

router.post('/messages/announcement', authController.authenticate, authController.authenticateCoordinator, announcementController.postAnnouncement);
router.get('/messages/announcement', announcementController.getAnnouncements);

router.post('/messages/private', authController.authenticate, privateChatController.postMessage);
router.get('/messages/private/:username1/:username2', authController.authenticate, privateChatController.getMessages);

router.post('/messages/share', authController.authenticate, shareController.postShare);
router.get('/messages/share', shareController.getShares);

router.get('/maps/hospitals', hospitalController.getHospitalNearby);
router.get('/maps/hospitals/estimates/time', hospitalController.getEstimateTime);
router.get('/maps/hospitals/estimates/price', hospitalController.getEstimatePrice);

router.get('/footprints/:username', footprintController.getFootprints);
router.post('/users/:username/footprintSetting', authController.authenticate, userController.updateShareFootprintsSetting);

router.get('/shareables', resourceSharingController.getAllShareableResources);
router.get('/users/:username/shareables', resourceSharingController.getShareableResourcesByUser);
router.post('/users/:username/shareables', authController.authenticate, resourceSharingController.postShareableResource);

router.post("/admin/users/:username/active", authController.authenticate, authController.authenticateAdmin, adminController.activateUser);
router.post("/admin/users/:username/inactive", authController.authenticate, authController.authenticateAdmin, adminController.deactivateUser);
router.post("/admin/users/:username", authController.authenticate, authController.authenticateAdmin, adminController.updateUser);
router.get("/admin/users", authController.authenticate, authController.authenticateAdmin, adminController.getAllUsers);

exports.router = router;
